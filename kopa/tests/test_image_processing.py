import os
import unittest
from kopa.clubs.doctype.jugador.jugador import Jugador

class TestImageProcessing(unittest.TestCase):
    def setUp(self):
        # Setup a test instance of Jugador
        self.jugador = Jugador()
        self.test_image_path = 'path/to/test/image.jpg'  # Replace with actual test image path

    def test_image_processing(self):
        # Simulate setting the photo attribute
        self.jugador.foto = self.test_image_path
        
        # Call the validate method to trigger image processing
        self.jugador.validate()
        
        # Check if the processed image exists and is under 17KB
        processed_image_size = os.path.getsize(self.test_image_path)
        self.assertLessEqual(processed_image_size, 17 * 1024, "Processed image exceeds 17KB")

if __name__ == '__main__':
    unittest.main()
