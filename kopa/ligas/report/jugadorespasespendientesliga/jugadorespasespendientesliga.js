
function aprobar(codigo){
    frappe.confirm('¿Esta seguro de continuar con la aprobación del Pase? '+ codigo,
    () => {
        frappe.call({
            method: "kopa.ligas.report.jugadorespasespendientesliga.jugadorespasespendientesliga.Aprobar",
            args: {
                codigo: codigo,
            },
            callback: (r) => {
               	
                 if (r.message == 1)
                 frappe.set_route("Form", "PaseAprobadoFedeprobat", codigo);

               },
        });
    }, () => {
        // action to perform if No is selected
    })
}