import frappe

@frappe.whitelist()
def Aprobar(codigo):
    values = {'name': codigo}
    jpp = frappe.db.sql(""" select * from tabPasesAprobadosLiga tpal where tpal.name = %(name)s """,values=values,as_dict=1)#frappe.get_doc("PasesAprobadosLiga",codigo)
    ent = frappe.new_doc("PaseAprobadoFedeprobat")
    ent.liga = jpp[0].liga
    ent.club = jpp[0].club
    ent.clubdestino = jpp[0].clubdestino
    ent.ci = jpp[0].ci
    ent.jugador = jpp[0].jugador
    ent.codigo = codigo
    ent.insert()
    msg ="""Se ha Geneardo el registro, para completar la aprobación, debe validar el documento:  <a href="/app/pasesaprobadosliga/{0}">{0}</a>  """.format(ent.name)
    return 1 
    