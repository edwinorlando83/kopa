
function irajugador(ci){
    frappe.confirm('¿Ir al registro del Jugador '+ci+' ?',
    () => {
               	
        frappe.set_route("Form", "Jugador", ci);
        
    }, () => {
        // action to perform if No is selected
    })
}
function aprobar(codigo){
    frappe.confirm('¿Esta seguro de continuar con la aprobación del Pase? '+codigo,
    () => {
        frappe.call({
            method: "kopa.ligas.report.jugadorespasespendientes.jugadorespasespendientes.Aprobar",
            args: {
                codigo: codigo,
            },
            callback: (r) => {
               	
                 if (r.message == 1)
                 frappe.set_route("Form", "PasesAprobadosLiga", codigo);
               },
        });
    }, () => {
        // action to perform if No is selected
    })
}