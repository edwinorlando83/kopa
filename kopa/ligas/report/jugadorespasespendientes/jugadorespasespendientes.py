import frappe

@frappe.whitelist()
def Aprobar(codigo):
    jpp = frappe.get_doc("JugadoresPasePendiente",codigo)
    ent = frappe.new_doc("PasesAprobadosLiga")
    ent.federacion = jpp.federacion
    ent.liga_origen = jpp.liga
    ent.club_origen = jpp.club
    ent.federacion_destino = jpp.federacion_destino
    ent.liga_destino = jpp.liga_destino
    ent.club_destino = jpp.club_destino
    ent.ci = jpp.ci
    ent.jugador = jpp.jugador
    ent.codigo = codigo
    ent.insert()
    msg ="""Se ha Geneardo el registro, para completar la aprobación, debe validar el documento:  <a href="/app/pasesaprobadosliga/{0}">{0}</a>  """.format(ent.name)
    return 1 
    