// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on("Fixture", {
  onload: function (frm) {
    frm.set_query("campeonato", function () {
      return {
        filters: [
          ["liga", "=", cur_frm.doc.liga],
          ["activo", "=", "1"],
        ],
      };
    });
    frm.set_query("categoria", function () {
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
      };
    });
    frm.set_query("grupo", function () {
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
      };
    });
    frm.set_query("fase", function () {
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
      };
    });
    frm.set_query("club_local", "calendario", function () {
      return {
        filters: [
          ["liga", "=", cur_frm.doc.liga],
          ["nombrecategoria", "=", cur_frm.doc.nombrecat],
        ],
      };
    });
    frm.set_query("club_visitante", "calendario", function () {
      return {
        filters: [
          ["liga", "=", cur_frm.doc.liga],
          ["nombrecategoria", "=", cur_frm.doc.nombrecat],
        ],
      };
    });
    frm.set_query("codigodisciplina", function () {
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
      };
    });
  },

  tipo_fixture: function (frm) {
    if (frm.doc.tipo_fixture == "AUTOMATICO IA") {
      let teams = [];
      frappe
        .call({
          method: "kopa.kopa.api.get_clubs_campeonato_Cat_grupo_ia",
          args: {
            federacion:cur_frm.doc.federacion,
            liga: cur_frm.doc.liga,
            campeonato: cur_frm.doc.campeonato,
            categoria: cur_frm.doc.categoria,
            grupo: cur_frm.doc.grupo,
            fase: cur_frm.doc.fase,
            disciplina: cur_frm.doc.codigodisciplina,
          },
        })
        .done((r) => {
          console.log(r.message);
          teams = r.message;
          frappe
            .call({
              method: "kopa.kopa.api.generar_calendario_ia",
              args: { clubs: JSON.stringify(teams) },
            })
            .done((r) => {
              console.log(r.message)
              cur_frm.clear_table("calendario");
              frm.doc.jugadores = [];
              let numero_fecha = 1;
              $.each(r.message, function (_i, e) {
                let i = 0;
                for (let j = 0; j < e.length; j++) {
                  //frm.add_child('calendario', { local: e., valor: 0, tiempo: 0, total: 0 });
                  let entry = frm.add_child("calendario");
                  entry.club_local = e[i][0][0];
                  entry.club_visitante = e[i][1][0];
                  entry.numero_fecha = numero_fecha;
                  i++;
                }
                numero_fecha++;
              });
              refresh_field("calendario");
            });
        });
    }
    if (frm.doc.tipo_fixture == "AUTOMATICO") {
      let teams = [];
      frappe
        .call({
          method: "kopa.kopa.api.get_clubs_campeonato_Cat_grupo",
          args: {
            federacion:cur_frm.doc.federacion,
            liga: cur_frm.doc.liga,
            campeonato: cur_frm.doc.campeonato,
            categoria: cur_frm.doc.categoria,
            grupo: cur_frm.doc.grupo,
            fase: cur_frm.doc.fase,
            disciplina: cur_frm.doc.codigodisciplina,
          },
        })
        .done((r) => {
        
          teams = r.message;
          frappe
            .call({
              method: "kopa.kopa.api.crear_fixture",
              args: { clubs: JSON.stringify(teams) },
            })
            .done((r) => {
              cur_frm.clear_table("calendario");
              frm.doc.jugadores = [];
              let numero_fecha = 1;
              $.each(r.message, function (_i, e) {
                let i = 0;
                for (let j = 0; j < e.length; j++) {
                  //frm.add_child('calendario', { local: e., valor: 0, tiempo: 0, total: 0 });
                  let entry = frm.add_child("calendario");
                  entry.club_local = e[i][0][0];
                  entry.club_visitante = e[i][1][0];
                  entry.numero_fecha = numero_fecha;
                  i++;
                }
                numero_fecha++;
              });
              refresh_field("calendario");
            });
        });
    } else {
      console.log("fixture manual");
      cur_frm.clear_table("calendario");
      refresh_field("calendario");
    }
  },
  refresh: function (frm) {},
});

frappe.ui.form.on("Fixture", "onload", function (frm) {
  /*frm.fields_dict[jugadores].grid.get_field('calendario').get_query =
    function() {
        return {
            query: "kopa.kopa.api.crear_fixture",
            filters: {
            "club":cur_frm.doc.club
            }
        }
    } */
});
