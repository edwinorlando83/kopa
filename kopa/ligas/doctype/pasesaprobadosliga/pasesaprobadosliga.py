# Copyright (c) 2022, kopa and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class PasesAprobadosLiga(Document):
	def on_update(self):
			if self.docstatus ==1:
				frappe.db.set_value('JugadoresPasePendiente', self.codigo, 'aprobadoliga', 1)