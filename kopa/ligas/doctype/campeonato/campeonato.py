# Copyright (c) 2022, kopa and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class Campeonato(Document):
	def on_update(self):
		values = {'name': self.name, 'liga':self.liga,'federacion':self.federacion}
		#data = frappe.db.sql(""" SELECT count(name) FROM `tabSanciones` s	WHERE s.ci = %(ci)s and docstatus = 0""", values=values, as_dict=0)
		frappe.db.sql(""" UPDATE `tabCampeonato` tc SET tc.activo = 0 WHERE tc.federacion = %(federacion)s and tc.liga = %(liga)s AND tc.docstatus=1 """, values=values, as_dict=0)

	#def on_validate(self):
	#	values = {'name': self.name, 'liga':self.liga,'federacion':self.federacion}
	#	#data = frappe.db.sql(""" SELECT count(name) FROM `tabSanciones` s	WHERE s.ci = %(ci)s and docstatus = 0""", values=values, as_dict=0)
	#	frappe.db.sql(""" UPDATE `tabCampeonato` tc SET tc.activo = 0 WHERE tc.federacion = %(federacion)s and tc.liga = %(liga)s AND tc.docstatus=1 """, values=values, as_dict=0)
		
