# Copyright (c) 2022, kopa and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class Sanciones(Document):
	def on_update(self):
		values = {'ci': self.ci}
		#data = frappe.db.sql(""" SELECT count(name) FROM `tabSanciones` s	WHERE s.ci = %(ci)s and docstatus = 0""", values=values, as_dict=0)
		data = frappe.db.count('Sanciones', {'ci': self.ci, 'docstatus':0})
		self.informe_vocal = "{0}".format(data)
		if self.docstatus == 0: 
			frappe.db.set_value('Jugador', self.ci, 'con_sancion', 1)
			frappe.db.set_value('Jugador', self.ci, 'fecha_inicial', self.fecha_inicio_sancion)
			frappe.db.set_value('Jugador', self.ci, 'fecha_final',self.fecha_fin_sancion)
			frappe.db.set_value('Jugador', self.ci, 'fecha_rebaja_de_sancion',self.fecha_rebaja_sancion)
		if "{0}".format(data) == "0" and self.docstatus == 1: 
			frappe.db.set_value('Jugador', self.ci, 'con_sancion', 0)
			getname = frappe.db.get_value("JugadorClub",{'jugador':self.ci, 'con_sancion':1},"name")
			frappe.db.set_value('JugadorClub', getname, 'con_sancion', 0)