// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('campeonato_categorias', {
	onload: function (frm) {
		frm.set_query("categoria", function () {
			return {
				filters: [
					['liga', '=', cur_frm.doc.liga]
				]
			};
		});

	},
	refresh: function (frm) {
		
	}
});
