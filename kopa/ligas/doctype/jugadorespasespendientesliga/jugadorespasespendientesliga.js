// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('JugadoresPasesPendientesLiga', {
	onload: function (frm) {

        frm.set_query("club", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.liga]
                ]
            };
        });
        frm.set_query("jugador", function () {
            return {
                filters: [
                    ['club', '=', cur_frm.doc.club]
                ]
            };
        });
    },
    refresh: function (frm) {


    }
});
