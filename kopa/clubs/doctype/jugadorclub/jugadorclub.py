# Copyright (c) 2022, kopa and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class JugadorClub(Document):
    def validate(self):
        if not self.is_new():
            old_doc = frappe.get_doc("JugadorClub", self.name)
            if old_doc.get("is_printed") and not frappe.user.has_role("FEDERACION CALIFICACION"):
                frappe.throw("No se puede modificar este registro porque el carnet ya ha sido impreso.")
