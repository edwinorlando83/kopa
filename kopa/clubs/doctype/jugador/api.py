import frappe

@frappe.whitelist()
def get_info_jugador(ci):
    values = {'ci': ci}
    return frappe.db.sql(""" SELECT tj.name as cedula, tj.apellidos as apellidos, tj.nombre as nombres, tcj.liga as liga, tcj.club club, tcj.campeonato campeonato from `tabJugador` tj, `tabClubJugadores` tcj, `tabJugadorClub` tjc  WHERE tj.name=%(ci)s and tj.name=tjc.jugador and tjc.parent=tcj.name """, values=values, as_dict=0)