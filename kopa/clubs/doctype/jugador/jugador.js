// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on("Jugador", {
    onload: function (frm) {
      let federacion=[];
      
      if (frappe.user.has_role("Administrador Club") && !frappe.user.has_role("System Manager")) {
        
        if(frappe.session.user!="Administrator"){
          frappe.call({
            method: "kopa.kopa.api.get_federacion1",
            args: {user:frappe.session.user}
              //freeze: true,
              
          }).done((f)=>{
              
              federacion=f.message[0];
              console.log(federacion[0]);
              //set_field_options("codigofederacion", federacion)
              
          });
          frm.set_query("liga", function () {
            return {
                filters: [
                    ['federacion', '=', cur_frm.doc.codigofederacion]
                  ]
              };
          });
        }
        
      }else{
        frappe.call({
          method: "kopa.kopa.api.get_federacion1",
            //freeze: true,
          args: {user:frappe.session.user}  
        }).done((f)=>{
          
            federacion=f.message[0];
            //set_field_options("codigofederacion", federacion[0])
            console.log(federacion[0]);
            
        });
        frm.set_query("liga", function () {
          return {
              filters: [
                  ['federacion', '=', cur_frm.doc.codigofederacion]
                ]
            };
        });
        frm.set_query("club", function () {
          return {
            filters: [["federacion","=",cur_frm.doc.codigofederacion],
            ["liga", "=", cur_frm.doc.liga]],
          };
        });
        frm.set_query("campeonato", function () {
          return {
            filters: [["liga", "=", cur_frm.doc.liga]],
          };
        });
        frm.set_query("jugador", function () {
          return {
            filters: [["liga", "=", cur_frm.doc.liga]],
          };
        });
  
      }
      
      
      
      /*frm.set_query("federacion", function () {
        return {
          filters: [["codigofederacion", "=", cur_frm.doc.codigofederacion]],
        };
      });*/
      
    },
    refresh: function (frm) {
      /*var now = moment();
      var total_days = moment(now).diff(frm.doc.fechanac, "days");
      var edadcalculada = Math.floor(total_days.toFixed(2) / 365.25);
      frm.set_value("edad", edadcalculada);
      var getfechajuvenil;
      let ligaa = frm.doc.lga;
      frappe.db
        .get_list("Campeonato", {
          fields: ["fecha_juveniles"],
          filters: {
            liga: ligaa,
            activo: 1,
          },
        })
        .then((r) => {
          if (r[0].fecha_juveniles) {
            console.log(r[0].fecha_juveniles);
            getfechajuvenil = r[0].fecha_juveniles;
            compareDates(frm.doc.fechanac, getfechajuvenil);
          }
        });
        
      const compareDates = (d1, d2) => {
        let date1 = new Date(d1).getTime();
        let date2 = new Date(d2).getTime();
        if (date1 < date2) {
          frm.set_value("esjuvenil", "");
        } else if (date1 > date2) {
          frm.set_value("esjuvenil", "JUVENIL");
        }
      };*/
      
      cur_frm.refresh_field(frm.doc.edad);
      if (frappe.user.has_role("Administrador Club")) {
        frm.set_df_property("aprobado", "read_only", 1);
        frm.set_df_property("observaciones", "read_only", 1);
        
        if (frm.doc.aprobado == 1) {
          frm.set_df_property("codigofederacion", "read_only", 1);
          frm.set_df_property("liga", "read_only", 1);
          frm.set_df_property("club", "read_only", 1);
          frm.set_df_property("apellidos", "read_only", 1);
          frm.set_df_property("nombre", "read_only", 1);
          frm.set_df_property("ci", "read_only", 1);
          frm.set_df_property("fechanac", "read_only", 1);
          frm.set_df_property("foto", "read_only", 1);
          frm.set_df_property("copia_cedula", "read_only", 0);
          frm.set_df_property("sexo", "read_only", 1);
          frm.set_df_property("direccion", "read_only", 1);
          frm.set_df_property("copia_cedula", "read_only", 1);
          frm.set_df_property("codigojugador", "read_only", 1);
          frm.set_df_property("exprofesional", "read_only", 1);
          frm.set_df_property("fecha_ultima_participacion", "read_only", 1);
          frm.set_df_property("carnet_vacunacion", "read_only", 1);
        }
      }
      //if(frappe.user.has_role("Administrador Club") || frappe.user.has_role("FEDEPROBAT")){
      if (frappe.user.has_role("COMISION CALIFICACION")) {
        frm.set_df_property("codigofederacion", "read_only", 0);
        frm.set_df_property("liga", "read_only", 0);
        frm.set_df_property("club", "read_only", 0);
        frm.set_df_property("apellidos", "read_only", 1);
        frm.set_df_property("nombre", "read_only", 1);
        frm.set_df_property("direccion", "read_only", 1);
        frm.set_df_property("fechanac", "read_only", 1);
        frm.set_df_property("edad", "read_only", 1);
        frm.set_df_property("sexo", "read_only", 1);
        frm.set_df_property("foto", "read_only", 1);
        frm.set_df_property("copia_cedula", "read_only", 1);
        frm.set_df_property("nombrepadre", "read_only", 1);
        frm.set_df_property("nombremadre", "read_only", 1);
        frm.set_df_property("aprobado", "read_only", 0);
        frm.set_df_property("codigojugador", "read_only", 1);
        frm.set_df_property("exprofesional", "read_only", 1);
        frm.set_df_property("fecha_ultima_participacion", "read_only", 1);
        frm.set_df_property("observaciones", "read_only", 1);
      }
      if (frappe.user.has_role("FEDERACION CALIFICACION")) {
        frm.set_df_property("codigofederacion", "read_only", 0);
        frm.set_df_property("liga", "read_only", 0);
        frm.set_df_property("club", "read_only", 0);
        frm.set_df_property("apellidos", "read_only", 1);
        frm.set_df_property("nombre", "read_only", 1);
        frm.set_df_property("direccion", "read_only", 1);
        frm.set_df_property("fechanac", "read_only", 1);
        frm.set_df_property("edad", "read_only", 1);
        frm.set_df_property("sexo", "read_only", 1);
        frm.set_df_property("foto", "read_only", 1);
        frm.set_df_property("copia_cedula", "read_only", 0);
        frm.set_df_property("nombrepadre", "read_only", 1);
        frm.set_df_property("nombremadre", "read_only", 1);
        frm.set_df_property("aprobado", "read_only", 0);
        frm.set_df_property("codigojugador", "read_only", 1);
        frm.set_df_property("exprofesional", "read_only", 0);
        frm.set_df_property("fecha_ultima_participacion", "read_only", 0);
        frm.set_df_property("observaciones", "read_only", 0);
      }
      if (frappe.user.has_role("PRESIDENTE")) {
        frm.set_df_property("codigofederacion", "read_only", 1);
        frm.set_df_property("liga", "read_only", 0);
        frm.set_df_property("club", "read_only", 0);
        frm.set_df_property("apellidos", "read_only", 0);
        frm.set_df_property("nombre", "read_only", 0);
        frm.set_df_property("direccion", "read_only", 0);
        frm.set_df_property("fechanac", "read_only", 0);
        frm.set_df_property("edad", "read_only", 1);
        frm.set_df_property("sexo", "read_only", 0);
        frm.set_df_property("foto", "read_only", 0);
        frm.set_df_property("copia_cedula", "read_only", 0);
        frm.set_df_property("nombrepadre", "read_only", 0);
        frm.set_df_property("nombremadre", "read_only", 0);
        frm.set_df_property("aprobado", "read_only", 0);
        frm.set_df_property("codigojugador", "read_only", 1);
        frm.set_df_property("exprofesional", "read_only", 0);
        frm.set_df_property("fecha_ultima_participacion", "read_only", 0);
        frm.set_df_property("observaciones", "read_only", 0);
      }
      if (frappe.user.has_role("Administrador Liga")) {
        frm.set_df_property("codigofederacion", "read_only", 0);
        frm.set_df_property("liga", "read_only", 0);
        frm.set_df_property("club", "read_only", 0);
        frm.set_df_property("apellidos", "read_only", 0);
        frm.set_df_property("nombre", "read_only", 0);
        frm.set_df_property("direccion", "read_only", 0);
        frm.set_df_property("fechanac", "read_only", 0);
        frm.set_df_property("edad", "read_only", 1);
        frm.set_df_property("sexo", "read_only", 0);
        frm.set_df_property("foto", "read_only", 0);
        frm.set_df_property("copia_cedula", "read_only", 0);
        frm.set_df_property("nombrepadre", "read_only", 0);
        frm.set_df_property("nombremadre", "read_only", 0);
        frm.set_df_property("aprobado", "read_only", 1);
        frm.set_df_property("codigojugador", "read_only", 1);
        frm.set_df_property("exprofesional", "read_only", 0);
        frm.set_df_property("fecha_ultima_participacion", "read_only", 0);
        frm.set_df_property("observaciones", "read_only", 0);
      }
      if (frappe.user.has_role("SECRETARIA")) {
        frm.set_df_property("codigofederacion", "read_only", 1);
        frm.set_df_property("liga", "read_only", 1);
        frm.set_df_property("club", "read_only", 1);
        frm.set_df_property("apellidos", "read_only", 1);
        frm.set_df_property("nombre", "read_only", 1);
        frm.set_df_property("direccion", "read_only", 1);
        frm.set_df_property("fechanac", "read_only", 1);
        frm.set_df_property("edad", "read_only", 1);
        frm.set_df_property("sexo", "read_only", 1);
        frm.set_df_property("foto", "read_only", 1);
        frm.set_df_property("copia_cedula", "read_only", 1);
        frm.set_df_property("nombrepadre", "read_only", 1);
        frm.set_df_property("nombremadre", "read_only", 1);
        frm.set_df_property("aprobado", "read_only", 1);
        frm.set_df_property("codigojugador", "read_only", 1);
        frm.set_df_property("exprofesional", "read_only", 1);
        frm.set_df_property("fecha_ultima_participacion", "read_only", 1);
        frm.set_df_property("observaciones", "read_only", 0);
      }
    },
});
