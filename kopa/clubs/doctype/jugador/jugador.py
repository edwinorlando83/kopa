# -*- coding: utf-8 -*-
# Copyright (c) 2018, kopa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from PIL import Image
from datetime import date
from rembg import remove
import io
import os

class Jugador(Document):
	def before_insert(self):
		self.nombres_completos  = self.apellidos + " " + self.nombre
		filtrojugador = {"ci":self.ci}
		#jugador = frappe.db.get_value('Jugador', {'name':(self.ci).replace("-","").replace(".","")}, ['liga','club'], as_dict=1)
		jugador= frappe.db.sql(""" SELECT tj.nombrefederacion,tj.codigofederacion, tj.liga, tj.club, REPLACE (tj.ci,"-","") 
									from tabJugador tj 
									WHERE REPLACE(tj.ci,"-","") = REPLACE (%(ci)s,"-","") """,values=filtrojugador,as_dict=1)
		#campeonato = frappe.db.get()
		
		if jugador:
			frappe.throw("El Jugador con CI: "+self.ci+", está registrado actualmente en el club: "+jugador[0].club+", en la Liga: " + jugador[0].liga +" ("+ str(jugador[0].nombrefederacion) + ").")
		
		if self.codigojugador!=None:
			vectorcodigo=self.codigojugador.split('L')
			#codigosiguiente=int(vectorcodigo[1])
			cuentacodigo=len(vectorcodigo[1])
			#frappe.throw("El Jugador con CI: "+cuentacodigo+", está jugando actualmente en el club: " + str(codigosiguiente) + ", en la Liga: ")
			if cuentacodigo==2:
				self.codigojugador='L00'+str(vectorcodigo[1])
			if cuentacodigo==3:
				self.codigojugador='L0'+str(vectorcodigo[1])
			if cuentacodigo==4:
				self.codigojugador='L'+str(vectorcodigo[1])
	
	def validate(self):
		# Validar y procesar la foto
		if self.foto:
			import frappe.utils.file_manager as file_manager
			import os
			
			# Verificar que el archivo sea un JPG
			if not self.foto.lower().endswith('.jpg'):
				frappe.throw("La foto debe ser un archivo JPG.")
			
			# Obtener el archivo original
			file_doc = frappe.get_doc("File", {"file_url": self.foto})
			file_path = os.path.join(frappe.get_site_path(), file_doc.file_url.strip('/'))
			
			# Verificar el tamaño del archivo
			file_size = file_doc.file_size
			
			# Establecer límite de 17KB
			max_file_size = 17 * 1024  # Límite de 17KB
			
			if file_size > max_file_size:
				frappe.throw(
					"El tamaño de la foto no puede exceder 17KB. "
					f"Tamaño actual: {file_size / 1024:.2f}KB"
				)
			
			# Procesar la imagen para cambiar el fondo a blanco
			if os.path.exists(file_path):
				try:
					# Abrir la imagen y remover el fondo
					with open(file_path, 'rb') as input_file:
						input_image = input_file.read()
						output_image = remove(input_image)
					
					# Guardar la imagen procesada
					with open(file_path, 'wb') as output_file:
						output_file.write(output_image)
					
					# Verificar el tamaño después de remover el fondo
					new_file_size = os.path.getsize(file_path)
					if new_file_size > max_file_size:
						# Comprimir la imagen si excede el tamaño máximo
						with Image.open(file_path) as img:
							# Convertir a RGB para asegurar fondo blanco
							if img.mode in ('RGBA', 'LA'):
								background = Image.new('RGB', img.size, 'white')
								background.paste(img, mask=img.split()[-1])
								img = background
							
							# Reducir calidad hasta lograr el tamaño deseado
							quality = 85
							while os.path.getsize(file_path) > max_file_size and quality > 20:
								img.save(file_path, 'JPEG', quality=quality)
								quality -= 5
							
							if os.path.getsize(file_path) > max_file_size:
								frappe.throw("No se pudo comprimir la imagen al tamaño requerido manteniendo una calidad aceptable")
				
				except Exception as e:
					frappe.throw(f"Error al procesar la imagen: {str(e)}")
			
		
		
	def on_update(self):
		jugador = frappe.db.get_value('Jugador', {'name':self.ci}, ['codigojugador'], as_dict=1)
		#campeonato = frappe.db.get_list("Campeonato", filters={'liga':self.liga,'activo':1},fields={'name','fecha_juveniles'})
		if self.edad == 0:
			str_fechanacimiento = self.fechanac.split("-")
			fechanacimiento=date(int(str_fechanacimiento[0]),int(str_fechanacimiento[1]),int(str_fechanacimiento[2]))
			#frappe.throw(str(str_fechanacimiento[0]))
			today = date.today()
			age = today.year - fechanacimiento.year - ((today.month, today.day) < (fechanacimiento.month, fechanacimiento.day))
			self.edad = age
		if jugador.codigojugador == None or jugador.codigojugador == "":
			maxcodigojugador=frappe.db.sql(""" SELECT max(codigojugador) FROM  tabJugador """)
			vectorcodigo=str(maxcodigojugador[0][0]).split('L')
			codigosiguiente=int(vectorcodigo[1])+1
			cuentacodigo=len(str(codigosiguiente))
			
			if cuentacodigo==2:
				self.codigojugador='L00'+str(codigosiguiente)
			if cuentacodigo==3:
				self.codigojugador='L0'+str(codigosiguiente)
			if cuentacodigo==4:
				self.codigojugador='L'+str(codigosiguiente)
			#jugador=frappe.db.set_value('Jugador', self.ci, 'codigojugador', self.codigojugador)
		#str_fechanacimiento = self.fechanac.split("-")
		#fechanacimiento=date(int(str_fechanacimiento[0]),int(str_fechanacimiento[1]),int(str_fechanacimiento[2]))
		#if campeonato[0].fecha_juveniles<=fechanacimiento:
		#	self.esjuvenil="JUVENIL"
		#else:
		#	self.esjuvenil="NO JUVENIL"
	#	frappe.db.set(self, 'status', 'Jugador')
