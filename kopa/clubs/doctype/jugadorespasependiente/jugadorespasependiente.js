// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('JugadoresPasePendiente', {
	onload: function (frm) {
        /*let federacion=[];
        frappe.call({
            method: "kopa.kopa.api.get_federacion",
            //freeze: true,
            
        }).done((f)=>{
            console.log(f.message);
            federacion=f.message;
            set_field_options("federacion_destino", federacion)
            set_field_options("federacion", federacion)
            
        });   */     
        frm.set_query("codigo_liga", function () {
            return {
                filters: [
                    ['federacion', '=', cur_frm.doc.federacion]
                ]
            };
        });
        frm.set_query("codigo_club", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.codigo_liga]
                ]
            };
        });
        frm.set_query("jugador", function () {
            return {
                filters: [
                    ['club', '=', cur_frm.doc.club]
                ]
            };
        });
        
        frm.set_query("codigo_liga_destino", function () {
            return {
                filters: [
                    ['federacion', '=', cur_frm.doc.federacion_destino]
                ]
            };
        });
        frm.set_query("codigo_club_destino", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.codigo_liga_destino]
                ]
            };
        });
        /*frm.set_query("clubdestino", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.liga_destino]
                ]
            };
        });*/
        
    },
    refresh: function (frm) {
        if (frappe.user.has_role("Administrador Club")) {
            frm.set_df_property("inscrito", "read_only", 1);
            
          }
          if (frappe.user.has_role("Administrador Liga")) {
            frm.set_df_property("inscrito", "read_only", 0);
          }
          if (frappe.user.has_role("COMISION TECNICA")) {
            frm.set_df_property("inscrito", "read_only", 0);
          }
        
        

    }
});
