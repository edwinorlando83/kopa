# -*- coding: utf-8 -*-
# Copyright (c) 2018, kopa and contributors
# For license information, please see license.txt
# Version 15

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.utils import getdate, today
from datetime import datetime


class ClubJugadores(Document):
    def get_categorias(self):
        return frappe.db.sql("""SELECT nombre FROM  tabGrupo """)

    def before_submit(self):
        
        contar = len(self.jugadores)
        values = frappe.db.get_value('Campeonato', {'name': self.campeonato, 'activo': 1}, [
            'liga', 'f_inicio_de_inscripciones', 'f_fin_de_inscripciones'], as_dict=1)
        today = date.today()
        lista = []
        contar = 0
        for e in self.jugadores:
            lista.append(e.numero)
            contar += 1
        frappe.throw(str(contar))

    def validate(self):
        contar = 0  # len(self.jugadores)
        contar_nom_jug_fechanac = 0
        cuentaextranjeros = 0
        values = frappe.db.get_value('Campeonato', {'name': self.campeonato, 'activo': 1}, [
            'liga', 'f_inicio_de_inscripciones', 'f_fin_de_inscripciones', 'numero_jugadores', 'tipo_liga', 'no_num_camiseta', 'num_extranjeros','fecha_nac_min_jug','anios_descanso_exprof','num_juveniles','fecha_juveniles'], as_dict=1)
        today = date.today()
        lista = []
        suma = 0
        for e in self.jugadores:
            lista.append({"numero": e.numero, "darbaja": e.darbaja,"f_nac":e.fnacimiento})
        result_no_repetidos = []
        if values.no_num_camiseta:
            suma = 0
        else:
            x = 0
            for element in lista:

                if element["darbaja"] == 0:
                    if element["numero"] not in result_no_repetidos:
                        result_no_repetidos.append(element["numero"])
                    else:
                        suma += 1
                x += 1
            # frappe.throw("baja paso: "+str(result_no_repetidos))

        if suma > 0:
            frappe.throw("Error: Existen números de camisetas repetidos.")

        listaNumJugadores = []
        for e in self.jugadores:
            str_fechanacimiento = str(e.fnacimiento).split("-")
            str_fechajuveniles=str(values.fecha_juveniles).split("-")
            fechanacimiento=date(int(str_fechanacimiento[0]),int(str_fechanacimiento[1]),int(str_fechanacimiento[2]))
            fechajuveniles=date(int(str_fechajuveniles[0]),int(str_fechajuveniles[1]),int(str_fechajuveniles[2]))
			#frappe.throw(str(str_fechanacimiento[0]))
            today = date.today()
            age = today.year - fechanacimiento.year - ((today.month, today.day) < (fechanacimiento.month, fechanacimiento.day))
            aniosdescanso=0
            if e.f_ultima_p:
                str_fechanacimiento = str(e.f_ultima_p).split("-")
                fechanacimiento=date(int(str_fechanacimiento[0]),int(str_fechanacimiento[1]),int(str_fechanacimiento[2]))
    			#frappe.throw(str(str_fechanacimiento[0]))
                today = date.today()
                aniosdescanso = today.year - fechanacimiento.year - ((today.month, today.day) < (fechanacimiento.month, fechanacimiento.day))               
            
            strjuvenil=""
            if fechanacimiento>=fechajuveniles:
                strjuvenil="JUVENIL"
            else:
                strjuvenil="NO JUVENIL"
			
            listaNumJugadores.append(
                {"darbaja": str(e.darbaja), "edad": str(age), "nacional": e.nacional,"f_nac":e.fnacimiento,"juvenil":strjuvenil,"aniosexprof":aniosdescanso,"exprofesional":e.exprofesional})

        if values.tipo_liga == "Parroquial":
            for element in range(len(listaNumJugadores)):
                if int(listaNumJugadores[element]["darbaja"]) == 0 and int(listaNumJugadores[element]["edad"]) >= 18:
                    contar = contar+1
                if listaNumJugadores[element]["nacional"] == "Extranjero":
                    cuentaextranjeros = cuentaextranjeros+1
            for element in range(len(listaNumJugadores)):
                if listaNumJugadores[element]["f_nac"] <= values.fecha_nac_min_jug:
                    contar_nom_jug_fechanac = contar_nom_jug_fechanac + 1
            if contar > values.numero_jugadores:
                frappe.throw("Error: Solo puede inscribir hasta "+str(values.numero_jugadores) +
                             " jugadores mayores de 18 años. (Ud tiene "+str(contar)+" Jugadores mayores a 18 años)")
            if contar_nom_jug_fechanac >0:
                frappe.throw("Advertencia: Solo jugaoresr que hayan nacido hasta la fecha "+str(values.fecha_nac_min_jug) +
                             ". (Ud tiene "+str(contar_nom_jug_fechanac)+" Jugadores que han nacido luego de es fecha.)")
            if cuentaextranjeros > values.num_extranjeros:
                frappe.throw("Error: Solo puede inscribir hasta "+str(values.num_extranjeros) +
                             " jugadores extranjeros. (Ud tiene "+str(cuentaextranjeros)+" Jugadores extranjeros)")

        
        if values.tipo_liga == "Barrial":
            
            contarjugexp_vigentes=0
            contarnumjuvenil=0
            for element in range(len(listaNumJugadores)):
                if int(listaNumJugadores[element]["darbaja"]) == 0:
                    contar = contar + 1
            for element in range(len(listaNumJugadores)):
                
                if int(listaNumJugadores[element]["aniosexprof"]) < int(values.anios_descanso_exprof) and int(listaNumJugadores[element]["exprofesional"])==1:
                    contarjugexp_vigentes = contarjugexp_vigentes + 1
                    
                if listaNumJugadores[element]["f_nac"] >= values.fecha_nac_min_jug:
                    contar_nom_jug_fechanac = contar_nom_jug_fechanac + 1
                
                if int(listaNumJugadores[element]["edad"]) < 18:
                    contarnumjuvenil = contarnumjuvenil + 1
                    
            if contar > values.numero_jugadores:
                frappe.throw("Advertencia: Solo puede inscribir hasta "+str(values.numero_jugadores) +
                             " jugadores. (Ud tiene "+str(contar)+" Jugadores.)")
            if contar_nom_jug_fechanac > 0:
                frappe.throw("Advertencia: Solo jugadores que hayan nacido hasta la fecha "+str(values.fecha_nac_min_jug) +
                             ". (Ud tiene "+str(contar_nom_jug_fechanac)+" Jugadores que han nacido luego de es fecha.)")
            if contarjugexp_vigentes>0:
                frappe.throw("Advertencia: No se puede registrar jugadores exprofesionales que no hayan descansado "+str(values.anios_descanso_exprof) +
                             " años. (Ud tiene "+str(contarjugexp_vigentes)+" Jugador(es) exprofesional(es) en su lista.)")
            if contarnumjuvenil<int(values.num_juveniles):
                frappe.throw("Advertencia: Debe registrar al menos "+str(values.num_juveniles) +
                             "juveniles. (Ud tiene "+str(contarnumjuvenil)+" Jugadores juveniles en su lista.)")
    
        if getdate(today) < getdate(values.f_inicio_de_inscripciones) or getdate(today) > getdate(values.f_fin_de_inscripciones):
            frappe.throw(
                "Error: Ud no puede Inscribirse o agregar mas jugadores porque esta fuera de las fechas límite.")
