// Copyright (c) 2018, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on("ClubJugadores", {
  //frm.fields_dict[‘child_table_name’].grid.wrapper.find(‘.grid-insert-row-below’).hide();
  //frm.fields_dict[‘child_table_name’].grid.wrapper.find(‘.grid-insert-row’).hide();
  before_jugadores_remove: function(frm, cdt, cdn) {
      var row = locals[cdt][cdn];
      if (row.is_printed && !frappe.user.has_role('FEDERACION CALIFICACION')) {
          frappe.throw('No se puede eliminar este registro porque el carnet ya ha sido impreso.');
          return false;
      }
  },
  onload: function (frm) {
    
    
    if (frappe.user.has_role("Administrador Club")) {
      frm.set_df_property("grupo", "read_only", 1);
      frm.set_df_property("fase", "read_only", 1);
      frm.set_df_property("tabsanciones", "read_only", 1);
    }
    if (frappe.user.has_role("Administrador Liga")) {
      frm.set_df_property("tabsanciones", "read_only", 0);
      frm.set_df_property("fase", "read_only", 0);
      frm.set_df_property("grupo", "read_only", 0);
    }
    if (frappe.user.has_role("COMISION TECNICA")) {
      frm.set_df_property("grupo", "read_only", 0);
      frm.set_df_property("fase", "read_only", 0);
      frm.set_df_property("tabsanciones", "read_only", 0);
      frm.set_df_property("fase", "read_only", 0);
    }
    /*let clobb = cur_frm.doc.club;
        var arrJugadores = {};
        console.log(clobb);
        frm.fields_dict['jugadores'].grid.get_field('jugador').get_query =
        function() {
            frappe.call({
                method:"kopa.kopa.api.filtroJugadoresxClub",
                args: {club:clobb}
            }).done((r)=>{
                arrJugadores=r.message;
                console.log(r);
            });
            console.log(arrJugadores);
            return arrJugadores;
            
        }*/
    //frm.get_field("jugadores").grid.df.cannot_delete_rows = true;
    frm.set_df_property("jugadores", "cannot_delete_rows", 1);
    frm.fields_dict["jugadores"].grid.get_field("jugador").get_query =
      function () {
        return {
          query: "kopa.kopa.api.filtroJugadorxClub",
          filters: {
            club: cur_frm.doc.club,
          },
        };
      };

    frm.set_query("campeonato", function () {
      return {
        filters: [
          ["liga", "=", cur_frm.doc.liga],
          ["activo", "=", "1"],
        ],
      };
    });
    
    frm.set_query("liga", function () {
      return {
        filters: [["federacion", "=", cur_frm.doc.federacion]],
      };
    });
    
    frm.set_query("fase", function () {
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
      };
    });
    frm.set_query("disciplina", function () {
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
        filters: [["federacion", "=", cur_frm.doc.federacion]]
      };
    });

    frm.set_query("jugador", "jugadores", function (doc, cdt, cdn) {
      //const row = locals[cdt][cdn];
      return {
        filters: [["nombreclub", "=", cur_frm.doc.club]],
      };
    });

    frm.set_query("codigofase", "tabsanciones", function (doc, cdt, cdn) {
      //const row = locals[cdt][cdn];
      return {
        filters: [["liga", "=", cur_frm.doc.liga]],
      };
    });
    
  },
  refresh: function(frm) {
      //frm.fields_dict['jugadores'].grid.grid_rows.forEach(function(row) {
      cur_frm.fields_dict['jugadores'].grid.grid_rows.forEach(function(row) {
          if (row.doc.is_printed && !frappe.user.has_role('FEDERACION CALIFICACION')) {
            $(row.wrapper).find('input, select, textarea').prop('disabled', true);
            $(row.wrapper).css('pointer-events', 'none');
            console.log('Fila bloqueada:', row.doc.name);
          }
      });
      // Configurar el observador de la tabla
      frm.fields_dict['jugadores'].grid.wrapper.on('grid-row-render', function(e) {
        // Aplicar bloqueo a todas las filas existentes
        frm.fields_dict['jugadores'].grid.grid_rows.forEach(function(row) {
            if (row.doc.is_printed && !frappe.user.has_role('FEDERACION CALIFICACION')) {
                // Deshabilitar completamente la edición
                row.toggle_editable(false);
                
                // Deshabilitar todos los campos editables en la fila
                row.wrapper.find('input, select, textarea').prop('disabled', true);
                
                // Prevenir clics en la fila
                row.wrapper.css('pointer-events', 'none');
                
                // Ocultar botones de acción
                row.wrapper.find('.grid-delete-row, .grid-duplicate-row').hide();
                
                // Agregar indicador visual de bloqueo
                if (!row.wrapper.find('.lock-indicator').length) {
                    row.wrapper.find('.grid-row-check')
                        .after('<span class="lock-indicator" style="color: red; margin-left: 5px;">' +
                              '<i class="fa fa-lock"></i></span>');
                }
                
                // Agregar clase visual para filas bloqueadas
                row.wrapper.addClass('printed-row');
            }
        });
    });
  },
  jugadores_add: function(frm, cdt, cdn) {
    // Verificar si el usuario tiene el rol 'Manager'
    
  },
  validate: function(frm) {
    // Validación adicional antes de guardar
    /*frm.doc.jugadores.forEach(function(row) {
        // Verifica si la fila está marcada como impresa
        if (row.is_printed) {
            // Si el usuario no tiene el rol adecuado, lanza un error
            if (!frappe.user.has_role('FEDERACION CALIFICACION')) {
                frappe.validated = false;
                frappe.throw('No se pueden modificar registros que ya tienen el carnet impreso.');
            }
        }
    });*/
  }

});

frappe.ui.form.on('JugadorClub', {
  before_jugadores_remove: function(frm, cdt, cdn) {
      var row = locals[cdt][cdn];
      if (row.is_printed && !frappe.user.has_role('FEDERACION CALIFICACION')) {
          frappe.throw('No se puede eliminar este registro porque el carnet ya ha sido impreso.');
          return false;
      }
  },
  
  form_render: function(frm, cdt, cdn) {
      // Asegurar que los campos estén bloqueados después de cualquier renderizado
      var row = locals[cdt][cdn];
      if (row.is_printed && !frappe.user.has_role('FEDERACION CALIFICACION')) {
          cur_frm.fields_dict['jugadores'].grid.grid_rows_by_docname[cdn].toggle_editable(false);
      }
  }
});

// Agregar estilos CSS para filas bloqueadas
frappe.dom.set_style(`
  .printed-row {
      background-color: #6d3b2a !important;
      opacity: 0.9;
  }
  .printed-row input, .printed-row select, .printed-row textarea {
      background-color: #6d3b2a !important;
      cursor: not-allowed !important;
  }
`);

/*frappe.ui.form.on("ClubJugadores", {
    clubliga: function(frm){
        let clobb = cur_frm.doc.club;
        var arrJugadores = {};
        console.log(clobb);
        frm.fields_dict["jugadores"].grid.get_field('jugador').get_query =
        function() {
            return {
                query: "kopa.kopa.api.filtroJugadorxClub",
                filters: {
                "club":cur_frm.doc.club
                }
            }
        }
        /*frm.fields_dict['jugadores'].grid.get_field('jugador').get_query =
        function() {
            console.log(clobb);
            frappe.call({
                method:"kopa.kopa.api.filtroJugadoresxClub",
                args: {club:clobb}
            }).done((r)=>{
                arrJugadores=r.message;
                console.log(r);
            });
            console.log(arrJugadores);
            //return arrJugadores;
            
        } 
    //}
    
});*/

/*frappe.ui.form.on(jugadores, {
    jugador:function (frm, cdt, cdn) {
    console.log("row remove");
    }
})*/
