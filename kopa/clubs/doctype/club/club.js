// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('Club', {
	onload: function (frm) {
	    frm.set_query("liga", function () {
            return {
                filters: [
                    ['federacion', '=', cur_frm.doc.federacion]
                ]
            };
        });
        frm.set_query("categoria", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.liga]
                ]
            };
        });
        frm.set_query("club", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.liga]
                ]
            };
        });
	},
	refresh: function(frm) {
		if(frappe.user.has_role("Administrador Club")){
            frm.set_df_property("categoria", "read_only", 1);
            frm.set_df_property("liga", "read_only", 1);
	    }
		if(frappe.user.has_role("Administrador Liga")){
            frm.set_df_property("categoria", "read_only", 0);
            frm.set_df_property("liga", "read_only", 0);
	    }
	}
});
