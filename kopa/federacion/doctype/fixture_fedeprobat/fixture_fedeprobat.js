// Copyright (c) 2023, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on("Fixture Fedeprobat", {
  // refresh: function(frm) {

  // }
  tipo_fixture: function (frm) {
    if (frm.doc.tipo_fixture == "AUTOMATICO") {
      let teams = [];
      console.log(cur_frm.disciplina);
      frappe
        .call({
          method: "kopa.kopa.api_federacion.get_clubs_campeonato_Cat_grupo",
          args: {
            federacion: cur_frm.doc.federacion,
            campeonato: cur_frm.doc.campeonato,
            grupo: cur_frm.doc.grupo,
            fase: cur_frm.doc.fase,
            disciplina: cur_frm.doc.disciplina,
          },
        })
        .done((r) => {
        console.log(r.message);
          teams = r.message;
          frappe
            .call({
              method: "kopa.kopa.api.crear_fixture",
              args: { clubs: JSON.stringify(teams) },
            })
            .done((r) => {
              cur_frm.clear_table("calendario");
              frm.doc.jugadores = [];
              let numero_fecha = 1;
              $.each(r.message, function (_i, e) {
                let i = 0;
                for (let j = 0; j < e.length; j++) {
                  //frm.add_child('calendario', { local: e., valor: 0, tiempo: 0, total: 0 });
                  let entry = frm.add_child("calendario");
                  entry.club_local = e[i][0][0];
                  entry.club_visitante = e[i][1][0];
                  entry.numero_fecha = numero_fecha;
                  i++;
                }
                numero_fecha++;
              });
              refresh_field("calendario");
            });
        });
    } else {
      console.log("fixture manual");
      cur_frm.clear_table("calendario");
      refresh_field("calendario");
    }
  },
  refresh: function (frm) {},
});
