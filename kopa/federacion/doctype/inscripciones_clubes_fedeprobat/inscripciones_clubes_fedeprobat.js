// Copyright (c) 2023, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('Inscripciones Clubes FEDEPROBAT', {
	onload: function (frm) {
    
    
		
		frm.set_df_property("listajugadores", "cannot_delete_rows", 1);
		
		frm.set_query("campeonato", function () {
		  return {
			filters: [
			  ["federacion", "=", cur_frm.doc.federacion],
			  ["activo", "=", "1"],
			],
		  };
		});
		frm.set_query("codigoclub", function () {
			return {
			  filters: [["federacion", "=", cur_frm.doc.federacion]],
			};
		  });
		frm.set_query("fase", function () {
		  return {
			filters: [["federacion", "=", cur_frm.doc.federacion]],
		  };
		});
		frm.set_query("disciplina", function () {
		  return {
			filters: [["federacion", "=", cur_frm.doc.federacion]]
		  };
		});
		frm.set_query("grupo", function () {
			return {
			  filters: [["federacion", "=", cur_frm.doc.federacion]],
			};
		  });
	
		frm.set_query("codigofase", "tabsanciones", function (doc, cdt, cdn) {
		  //const row = locals[cdt][cdn];
		  return {
			filters: [["federacion", "=", cur_frm.doc.federacion]],
		  };
		});
		
	  },
	  refresh: function (frm) {
		// Si el usuario tiene el rol, oculta el campo en la tabla secundaria
		if (frappe.user.has_role("Administrador Club")) {
		  
			if (frm.fields_dict.listajugadores && frm.fields_dict.listajugadores.grid) {
				// Asegurarse de que las filas están disponibles usando grid.refresh
				
				frm.fields_dict.listajugadores.grid.refresh();
		
				frm.fields_dict.listajugadores.grid.grid_rows.forEach(function(row) {
				  
					// Hacer que el campo 'para_imprimir' sea de solo lectura
					if (row.doc.para_imprimir !== undefined) {
					  
					  frm.set_df_property(`listajugadores.${row.idx}.para_imprimir`, 'read_only', 1);
					  console.log(frm);
					}
				});
			} else {
			  console.error('Campo jugadores no encontrado o no tiene la propiedad grid.');
		  }
		}
	  },
});
