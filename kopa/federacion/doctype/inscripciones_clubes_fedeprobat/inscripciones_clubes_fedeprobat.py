# Copyright (c) 2023, kopa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import date
from frappe.utils import getdate, today
from datetime import datetime

class InscripcionesClubesFEDEPROBAT(Document):
	def validate(self):
		contar = 0  # len(self.jugadores)
		contar_nom_jug_fechanac = 0
		cuentaextranjeros = 0
		values = frappe.db.get_value('Eventos Deportivos FEDEPROBAT', {'name': self.campeonato, 'activo': 1}, [
            'federacion', 'fechainicio', 'fechafin', 'cantidad_jugadores','fecha_min_nacimieto','fecha_max_nacimiento'], as_dict=1)
		today = date.today()
		lista = []
		suma = 0
		for e in self.listajugadores:
			lista.append({"numero": e.numero, "f_nacimiento":e.f_nacimiento})
		result_no_repetidos = []
		x = 0
		for element in lista:
			if element["numero"] not in result_no_repetidos:
				result_no_repetidos.append(element["numero"])
			else:
				suma += 1
			x += 1
            # frappe.throw("baja paso: "+str(result_no_repetidos))

		if suma > 0:
			frappe.throw("Error: Existen números de camisetas repetidos.")

		listaNumJugadores = []
		for e in self.listajugadores:
			str_fechanacimiento = str(e.f_nacimiento).split("-")
			#str_fechajuveniles=str(values.fecha_juveniles).split("-")
			fechanacimiento=date(int(str_fechanacimiento[0]),int(str_fechanacimiento[1]),int(str_fechanacimiento[2]))
			#fechajuveniles=date(int(str_fechajuveniles[0]),int(str_fechajuveniles[1]),int(str_fechajuveniles[2]))
			today = date.today()
			age = today.year - fechanacimiento.year - ((today.month, today.day) < (fechanacimiento.month, fechanacimiento.day))
			listaNumJugadores.append(
                { "edad": str(age), "f_nac":e.f_nacimiento})
                
		for element in range(len(listaNumJugadores)):    
			if listaNumJugadores[element]["f_nac"] < values.fecha_min_nacimieto:
				contar_nom_jug_fechanac = contar_nom_jug_fechanac + 1
		if contar_nom_jug_fechanac>0:
			frappe.throw("Existen jugadores que han nacido entes del "+str(values.fecha_min_nacimieto)+".")