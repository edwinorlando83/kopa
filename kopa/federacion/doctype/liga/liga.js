// Copyright (c) 2024, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('Liga', {
    refresh: function (frm) {
        if (frappe.user.has_role("Administrator")) {
            frm.set_df_property("solo_inscripcion", "read_only", 0);
		}
    }
});
