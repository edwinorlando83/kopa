# Copyright (c) 2024, kopa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document
import frappe


class Liga(Document):
	pass


def setliga():
    if frappe.db.exists("usuarios_ligas_equipo", frappe.session.user):
        ent = frappe.get_doc("usuarios_ligas_equipo", frappe.session.user)

        frappe.defaults.set_user_default("federacionuser", ent.federacion)
        if ent.liga:
            frappe.defaults.set_user_default("ligauser", ent.liga)
            if ent.club:
                frappe.defaults.set_user_default("clubuser", ent.club)
