// Copyright (c) 2023, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('Clubs Fedeprobat', {
	onload: function (frm) {        
        frm.set_query("nombreclub", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.liga]
                ]
            };
        });
        
    },
    refresh: function (frm) {
        if(frappe.user.has_role("Administrador Club")){
            frm.set_df_property("grupo", "read_only", 1);
	    }
        if(frappe.user.has_role("Administrador Liga")){
            frm.set_df_property("grupo", "read_only", 0);
	    }
    }
});
