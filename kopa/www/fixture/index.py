from __future__ import unicode_literals
import frappe
 
sitemap = 1

def get_context(context):
    context.show_search = True
    context.allow_guest = True
    context.no_breadcrumbs = True
    
@frappe.whitelist(allow_guest=True)
def fixture(liga,categoria,grupo,fase):
    campeonato = frappe.db.get_value("Campeonato", {'liga':liga,'activo':1})
    #frappe.throw(str(campeonato))
    values = {'liga': liga,'campeonato':campeonato,'categoria':categoria,'grupo':grupo,'fase':fase}
    return frappe.db.sql(""" SELECT tf.campeonato, tf.categoria, tf.grupo, tf.nombrecat , tc.`local`,
                         tc.visitante , tc.marcador_local ,tc.marcador_visitante, tc.fecha,
                         tc.hora, tc.nombreestadio,tc.numero_fecha
                         FROM tabFixture tf, tabCalendario tc 
                         WHERE tf.name = tc.parent AND 
                         tf.liga = %(liga)s AND 
                         tf.campeonato = %(campeonato)s AND 
                         tf.nombrecat = %(categoria)s AND 
                         tf.grupo = %(grupo)s AND 
                         tf.fase = %(fase)s 
                         ORDER BY tc.numero_fecha ASC """, values=values, as_dict=1)

@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
    values = {'liga': liga}
    categoriass = frappe.db.sql(""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)    
    return categoriass

@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
    values = {'liga': liga}
    gruposs = frappe.db.sql(""" SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
    return gruposs

@frappe.whitelist(allow_guest=True)
def get_fases(liga):
    values = {'liga': liga}
    fasess = frappe.db.sql(""" SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)    
    return fasess

@frappe.whitelist(allow_guest=True)
def get_ligas():
    
    ligas = frappe.db.sql(""" SELECT tl.name, tl.nombre  FROM tabLiga tl """, as_dict=1)    
    return ligas