from __future__ import unicode_literals
import frappe
 
sitemap = 1

 
def get_context(context):
	context.show_search = True
	context.allow_guest = True
	context.no_breadcrumbs = True


@frappe.whitelist(allow_guest=True)
def consulta_jugadores_rangoedad():
	#values = {'jugador': ci}
	sql =""" SELECT 
	CASE WHEN (tj.edad < 18) THEN 'MenoresA18' ELSE
		CASE WHEN (tj.edad BETWEEN 18 AND 40) THEN 'Entre18y40' ELSE
			CASE WHEN (tj.edad BETWEEN 40 AND 50) THEN 'Entre40y50' ELSE 
				CASE WHEN (tj.edad >= 50) THEN 'Mayoresa50'
				END
			END
		END
	END rango,
		COUNT(*) total,
		tcj.campeonato  campeonato
FROM tabJugador tj,tabJugadorClub tjc ,tabClubJugadores tcj, tabCampeonato tc
WHERE tj.name = tjc.jugador and 
tjc.parent =tcj .name AND 
tcj.campeonato =tc.name AND 
tc.activo =1
GROUP BY rango """
	return  frappe.db.sql(sql, as_dict=True)

@frappe.whitelist(allow_guest=True)
def get_ligas():
	ligas = frappe.db.sql(""" SELECT tl.name, tl.nombre  FROM tabLiga tl """, as_dict=1)    
	return ligas


@frappe.whitelist(allow_guest=True)
def consulta_jugadores_rangoedadXliga(liga):
	values = {'liga': liga}
	
	return  frappe.db.sql(""" SELECT 
	CASE WHEN (tj.edad < 18) THEN 'MenoresA18' ELSE
		CASE WHEN (tj.edad BETWEEN 18 AND 40) THEN 'Entre18y40' ELSE
			CASE WHEN (tj.edad BETWEEN 40 AND 50) THEN 'Entre40y50' ELSE 
				CASE WHEN (tj.edad >= 50) THEN 'Mayoresa50'
				END
			END
		END
	END rango,
		COUNT(*) total,
		tcj.campeonato  campeonato
FROM tabJugador tj,tabJugadorClub tjc ,tabClubJugadores tcj , tabCampeonato tc
WHERE tj.name = tjc.jugador and 
tjc.parent =tcj .name AND
tcj.liga =%(liga)s AND 
tcj.campeonato =tc.name AND 
tc.activo =1
GROUP BY rango """, values=values, as_dict=True)
	
	
	
@frappe.whitelist(allow_guest=True)
def getTablaSanciones(liga):
	values = {'liga': liga}
	#campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s """, values=values, as_dict=1)
	#values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
	#nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
	#								FROM tabFixture tf, tabCampeonato tc 
#									WHERE tf.liga = "LIGA DEPORTIVA BARRIAL LA PENINSULA" and 
#									tf.campeonato=tc.name  and
#									tc.activo =1 """, values=values, as_dict=1)
#	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
								tc.marcador_visitante,tc.numero_fecha, tc.jugado_diferido, tc.golesl,
								tc.goles_v, tc.tal, tc.ta_v, tf.numfechaactual
								FROM tabFixture tf ,tabCalendario tc, tabCampeonato tcam
								WHERE tf.liga = %(liga)s AND 
								tf.campeonato = tcam.name  AND 
								tcam.activo = 1 AND 
								tf.name = tc.parent """, values=values, as_dict=1)
	
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club,tcj.categoria 
									FROM tabClubJugadores tcj, tabCampeonato tcam
									WHERE tcj.liga = %(liga)s AND 
									tcj.campeonato=tcam.name  AND 
									tcam.activo = 1 
									order by tcj.club """, values=values, as_dict=1)
	nfixture = len(fixture)
	nresultados = len(tblResultados)
	sanciones = []
	
	for i in range(nfixture):
		if fixture[i].local!="DESCANSA" and fixture[i].visitante!="DESCANSA" and fixture[i].jugado_diferido==True:
			if fixture[i].numero_fecha<=fixture[0].numfechaactual:
				ta_x_partido=str(fixture[i].tal).split(",")
				if fixture[i].jugado_diferido==True:
					if str(fixture[i].tal)!="None": 
						for j in range(len(ta_x_partido)):
							total_sanciones=len(sanciones)
							if total_sanciones<1:
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha,"FechaActual":""})
							else:
								existe="NO"
								for k in range(total_sanciones):
									if str(fixture[i].local) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
										existe="SI"
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
										break
									else:
										existe="NO"
									sanciones[k]["Fecha"]=fixture[i].numero_fecha
								if existe=="NO":
									sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha})
					
     
				ta_x_partido=str(fixture[i].ta_v).split(",")
				if str(fixture[i].ta_v)!="None":
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[0]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha})
	nsanciones = len(sanciones) 
	
	for i in range(nsanciones):
		for j in range(nresultados):
			if tblResultados[j].club==sanciones[i]["Club"]:
				sanciones[i]["Categoria"] = tblResultados[j].categoria
	return sanciones


@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
    values = {'liga': liga}
    categoriass = frappe.db.sql(""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)    
    return categoriass