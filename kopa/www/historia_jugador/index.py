from __future__ import unicode_literals
import frappe
 
sitemap = 1

 
def get_context(context):
    context.show_search = True
    context.allow_guest = True
    context.no_breadcrumbs = True
    
@frappe.whitelist(allow_guest=True)
def historiajugador(ci):
    values = {'ci': ci}
    return frappe.db.sql(""" SELECT tj.name as cedula, tj.apellidos as apellidos, tj.nombre as nombres, tj.foto, tj.copia_cedula, tj.carnet_vacunacion, tcj.liga as liga, tcj.club club, tcj.campeonato campeonato 
from `tabJugador` tj
LEFT JOIN `tabJugadorClub` tjc
	on tj.name=tjc.jugador
left join `tabClubJugadores` tcj
	on tjc.parent=tcj.name
WHERE tj.name=%(ci)s """, values=values, as_dict=1)