from __future__ import unicode_literals
import frappe
 
sitemap = 1

def get_context(context):
	context.show_search = True
	context.allow_guest = True
	context.no_breadcrumbs = True

@frappe.whitelist(allow_guest=True)
def getTablaSanciones(liga,categoria,grupo,fase):
	values = {'liga': liga}
	campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.grupo=%(grupo)s and
								tf.fase=%(fase)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v,tc.tal,tc.ta_v,tc.trl,tc.tr_v
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club
							FROM tabClubJugadores tcj 
							WHERE tcj.liga = %(liga)s and 
							tcj.campeonato=%(campeonato)s and 
							tcj.categoria=%(categoria)s and
							tcj.grupo=%(grupo)s and
							tcj.fase=%(fase)s
							order by tcj.club """, values=values, as_dict=1)
	nfixture = len(fixture)
	nresultados = len(tblResultados)
 
	sanciones = []
	
	for j in range(nresultados):
		tblResultados[j].ta = 0
		tblResultados[j].tr = 0
	for i in range(nfixture):
		if fixture[i].local!="Descansa" and fixture[i].visitante!="Descansa" and fixture[i].jugado_diferido==True:
			if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
				ta_x_partido=str(fixture[i].tal).split(",")
				if fixture[i].jugado_diferido==True:
					if str(fixture[i].tal)!="None": 
						for j in range(len(ta_x_partido)):
							total_sanciones=len(sanciones)
							if total_sanciones<1:
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha,"FechaActual":""})
							else:
								existe="NO"
								for k in range(total_sanciones):
									if str(fixture[i].local) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
										existe="SI"
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
										break
									else:
										existe="NO"
									sanciones[k]["Fecha"]=fixture[i].numero_fecha
								if existe=="NO":
									sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha})
		
				ta_x_partido=str(fixture[i].ta_v).split(",")
				if str(fixture[i].ta_v)!="None":
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[0]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha})
				
				tr_x_partido=str(fixture[i].trl).split(",")
				if fixture[i].jugado_diferido==True:
					if str(fixture[i].trl)!="None": 
						for j in range(len(tr_x_partido)):
							total_sanciones=len(sanciones)
							if total_sanciones<1:
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","Fecha":fixture[i].numero_fecha,"FechaActual":""})
							else:
								existe="NO"
								for k in range(total_sanciones):
									if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
										existe="SI"
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
										break
									else:
										existe="NO"
									sanciones[k]["Fecha"]=fixture[i].numero_fecha
								if existe=="NO":
									sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","Fecha":fixture[i].numero_fecha})
		
				tr_x_partido=str(fixture[i].tr_v).split(",")
				if str(fixture[i].tr_v)!="None":
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"1","Fecha":fixture[i].numero_fecha})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","Fecha":fixture[i].numero_fecha})
	return sanciones

@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
	values = {'liga': liga}
	categoriass = frappe.db.sql(""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)    
	return categoriass

@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
	values = {'liga': liga}
	gruposs = frappe.db.sql(""" SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
	return gruposs

@frappe.whitelist(allow_guest=True)
def get_fases(liga):
	values = {'liga': liga}
	fasess = frappe.db.sql(""" SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)    
	return fasess


@frappe.whitelist(allow_guest=True)
def get_ligas():
	
	ligas = frappe.db.sql(""" SELECT tl.name, tl.nombre  FROM tabLiga tl """, as_dict=1)    
	return ligas