from __future__ import unicode_literals
import frappe
 
sitemap = 1

def get_context(context):
    context.show_search = True
    context.allow_guest = True
    context.no_breadcrumbs = True

@frappe.whitelist(allow_guest=True)
def fixture(liga,categoria,grupo,fase):
    campeonato = frappe.db.get_value("Campeonato", {'liga':liga,'activo':1})
    #frappe.throw(str(campeonato))
    values = {'liga': liga,'campeonato':campeonato,'categoria':categoria,'grupo':grupo,'fase':fase}
    return frappe.db.sql(""" SELECT tf.campeonato, tf.categoria, tf.grupo, tf.nombrecat , tc.`local`,
                         tc.visitante , tc.marcador_local ,tc.marcador_visitante, tc.fecha,
                         tc.hora, tc.estadio,tc.numero_fecha
                         FROM tabFixture tf, tabCalendario tc 
                         WHERE tf.name = tc.parent AND 
                         tf.liga = %(liga)s AND 
                         tf.campeonato = %(campeonato)s AND 
                         tf.nombrecat = %(categoria)s AND 
                         tf.grupo = %(grupo)s AND 
                         tf.fase = %(fase)s 
                         ORDER BY tc.numero_fecha ASC """, values=values, as_dict=1)

@frappe.whitelist(allow_guest=True)
def resultados(liga,categoria,grupo,fase):
    campeonato = frappe.db.get_value("Campeonato", {'liga':liga,'activo':1})
    #frappe.throw(str(campeonato))
    values = {'liga': liga,'campeonato':campeonato,'categoria':categoria,'grupo':grupo,'fase':fase}
    return frappe.db.sql(""" SELECT tcj.club, tcj.puntos,tcj.gol_a_favor,tcj.gol_en_contra
                        FROM tabClubJugadores tcj
                        WHERE tcj.liga = %(liga)s AND 
                        tcj.campeonato=%(campeonato)s AND 
                        tcj.categoria = %(categoria)s AND 
                        tcj.grupo= %(grupo)s AND 
                        tcj.fase = %(fase)s 
                        ORDER BY tcj.puntos DESC """, values=values, as_dict=1)

@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
    values = {'liga': liga}
    categoriass = frappe.db.sql(""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)    
    return categoriass

@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
    values = {'liga': liga}
    gruposs = frappe.db.sql(""" SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
    return gruposs

@frappe.whitelist(allow_guest=True)
def get_fases(liga):
    values = {'liga': liga}
    fasess = frappe.db.sql(""" SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)    
    return fasess

@frappe.whitelist(allow_guest=True)
def getTablaPosiciones(liga,categoria,grupo,fase):
    values = {'liga': liga}
    campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s and tc.activo=1 """, values=values, as_dict=1)
    values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
    nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
                                FROM tabFixture tf 
                                WHERE tf.liga = %(liga)s and 
                                tf.campeonato=%(campeonato)s and
                                tf.nombrecat=%(categoria)s and
                                tf.grupo=%(grupo)s and
                                tf.fase=%(fase)s """, values=values, as_dict=1)
    values = {'parent': nameFicture[0].name}
    fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
                            tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido
                            FROM tabCalendario tc 
                            WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
    values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
    tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club, tcj.puntos,tcj.gol_a_favor,tcj.gol_en_contra
                            FROM tabClubJugadores tcj 
                            WHERE tcj.liga = %(liga)s and 
                            tcj.campeonato=%(campeonato)s and 
                            tcj.categoria=%(categoria)s and
                            tcj.grupo=%(grupo)s and
                            tcj.fase=%(fase)s
                            order by tcj.club """, values=values, as_dict=1)
    nfixture = len(fixture)
    nresultados = len(tblResultados)
    for j in range(nresultados):
        tblResultados[j].puntos = 0
        tblResultados[j].gol_a_favor = 0
        tblResultados[j].gol_en_contra = 0
        tblResultados[j].partidos_jugados = 0
    for i in range(nfixture):
        
        if fixture[i].local!="DESCANSA" and fixture[i].visitante!="DESCANSA" and fixture[i].jugado_diferido==True:
            for j in range(nresultados):
                if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
                    if fixture[i].marcador_local==fixture[i].marcador_visitante:
                        if fixture[i].local==tblResultados[j].club:
                            tblResultados[j].puntos += 1
                            tblResultados[j].partidos_jugados += 1
    
                        if fixture[i].visitante==tblResultados[j].club:
                            tblResultados[j].puntos += 1
                            tblResultados[j].partidos_jugados += 1
                        
                    if fixture[i].marcador_local>fixture[i].marcador_visitante:
                        if fixture[i].local==tblResultados[j].club:
                            tblResultados[j].puntos += 3
                            tblResultados[j].partidos_jugados += 1
                        if fixture[i].visitante==tblResultados[j].club:
                            tblResultados[j].partidos_jugados += 1
            
                    if fixture[i].marcador_local<fixture[i].marcador_visitante:
                        if fixture[i].visitante==tblResultados[j].club:
                            tblResultados[j].puntos += 3
                            tblResultados[j].partidos_jugados += 1
                        if fixture[i].local==tblResultados[j].club:
                            tblResultados[j].partidos_jugados += 1

            for j in range(nresultados):
                if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
                    if fixture[i].local==tblResultados[j].club:
                        tblResultados[j].gol_a_favor += fixture[i].marcador_local
                        tblResultados[j].gol_en_contra += fixture[i].marcador_visitante
                    if fixture[i].visitante==tblResultados[j].club:
                        tblResultados[j].gol_a_favor += fixture[i].marcador_visitante
                        tblResultados[j].gol_en_contra += fixture[i].marcador_local
    return tblResultados
    
    