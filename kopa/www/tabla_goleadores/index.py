from __future__ import unicode_literals
import frappe
 
sitemap = 1

def get_context(context):
	context.show_search = True
	context.allow_guest = True
	context.no_breadcrumbs = True

@frappe.whitelist(allow_guest=True)
def getTablaGoleadores(liga,categoria,grupo,fase):
	values = {'liga': liga}
	campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.grupo=%(grupo)s and
								tf.fase=%(fase)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club
							FROM tabClubJugadores tcj 
							WHERE tcj.liga = %(liga)s and 
							tcj.campeonato=%(campeonato)s and 
							tcj.categoria=%(categoria)s and
							tcj.grupo=%(grupo)s and
							tcj.fase=%(fase)s
							order by tcj.club """, values=values, as_dict=1)
	nfixture = len(fixture)
	nresultados = len(tblResultados)
	goleadores = []
	
	for j in range(nresultados):
		tblResultados[j].puntos = 0
		tblResultados[j].gol_a_favor = 0
		tblResultados[j].gol_en_contra = 0
	for i in range(nfixture):
		if fixture[i].local!="DESCANSA" and fixture[i].visitante!="DESCANSA" and fixture[i].jugado_diferido==True:
			if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
				goloes_x_partido=str(fixture[i].golesl).split(",")
				if str(fixture[i].golesl)!="None":
					for j in range(len(goloes_x_partido)):
						total_goledores=len(goleadores)
						if total_goledores<1:
							goleadores.append({"Club":str(fixture[i].local),"Jugador":str(goloes_x_partido[0]),"Goles":"1"})
						else:
							existe="NO"
							for k in range(total_goledores):
								if str(fixture[i].local) == goleadores[k]["Club"] and str(goloes_x_partido[j]) == goleadores[k]["Jugador"]: 
									existe="SI"
									goleadores[k]["Goles"]=str(int(goleadores[k]["Goles"])+1)
									break
								else:
									existe="NO"
							if existe=="NO":
								goleadores.append({"Club":str(fixture[i].local),"Jugador":str(goloes_x_partido[j]),"Goles":"1"})
				goloes_x_partido=str(fixture[i].goles_v).split(",")
				if str(fixture[i].goles_v)!="None":
					for j in range(len(goloes_x_partido)):
						total_goledores=len(goleadores)
						if total_goledores<1:
							goleadores.append({"Club":str(fixture[i].visitante),"Jugador":str(goloes_x_partido[0]),"Goles":i})
						else:
							existe="NO"
							for k in range(total_goledores):
								if str(fixture[i].visitante) == goleadores[k]["Club"] and str(goloes_x_partido[j]) == goleadores[k]["Jugador"]: 
									existe="SI"
									goleadores[k]["Goles"]=str(int(goleadores[k]["Goles"])+1)
									break
								else:
									existe="NO"
							if existe=="NO":
								goleadores.append({"Club":str(fixture[i].visitante),"Jugador":str(goloes_x_partido[j]),"Goles":"1"})

	return goleadores

@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
    values = {'liga': liga}
    categoriass = frappe.db.sql(""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)    
    return categoriass

@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
    values = {'liga': liga}
    gruposs = frappe.db.sql(""" SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
    return gruposs

@frappe.whitelist(allow_guest=True)
def get_fases(liga):
    values = {'liga': liga}
    fasess = frappe.db.sql(""" SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)    
    return fasess

@frappe.whitelist(allow_guest=True)
def get_disciplinas(liga):
    values = {'liga': liga}
    fasess = frappe.db.sql(""" SELECT td.name,td.disciplina  FROM tabDisciplina td WHERE td.liga  = %(liga)s """, values=values, as_dict=1)    
    return fasess

@frappe.whitelist(allow_guest=True)
def get_ligas():
    
    ligas = frappe.db.sql(""" SELECT tl.name, tl.nombre  FROM tabLiga tl """, as_dict=1)    
    return ligas