// Copyright (c) 2022, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('Inscripciones Clubes FEDEPROBAT', {
	onload: function (frm) {
		frm.set_query("campeonato", function () {
            return {
                filters: [
                    ['liga', '=', cur_frm.doc.liga],
                    ['activo','=','1']
                ]
            };
        });
	}
});
