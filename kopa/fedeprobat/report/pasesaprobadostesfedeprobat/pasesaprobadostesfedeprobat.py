import frappe

@frappe.whitelist()
def Aprobar(codigo):
    pal = frappe.get_doc("PasesAprobadosLiga",codigo)
    ent = frappe.new_doc("PasesAprobadosFedeprobat")
    ent.federacion_origen = pal.federacion
    ent.liga_origen = pal.liga_origen
    ent.codigo_club_origen = pal.club
    ent.club_origen = pal.club_origen
    ent.federacion_destino = pal.federacion_destino
    ent.liga_destino = pal.liga_destino
    ent.codigo_club_destino = pal.codigo_club_destino
    ent.club_destino = pal.club_destino
    ent.ci = pal.ci
    ent.jugador = pal.jugador
    ent.codigo = codigo
    ent.insert()
    msg ="""Se ha Geneardo el registro, para completar la aprobación, debe validar el documento:  <a href="/app/pasesaprobadosfedeprobat/{0}">{0}</a>  """.format(ent.name)
    return 1
    