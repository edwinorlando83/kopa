import frappe


def get_permission_query_conditions_club(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' and name = '{2}')""".format(usuario.federacion,usuario.liga,usuario.club)


def get_permission_query_conditions_liga(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and name= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and name= '{1}' )""".format(usuario.federacion,usuario.liga)

def get_permission_query_conditions_federacion(user):
    #frappe.defaults.get_user_default('federacionuser')
    if not user: user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
      
        return """(name = '{0}' )""".format(usuario.federacion)


def get_permission_query_conditions_jugador(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        ligauser = frappe.defaults.get_user_default('ligauser')
        clubuser = frappe.defaults.get_user_default('clubuser')
        federacionuser = frappe.defaults.get_user_default('federacionuser')
        if federacionuser:
            if ligauser:
                if clubuser:
                    return """(tabJugador.club  = '{0}' and tabJugador.liga = '{1}' ) and codigofederacion = '{2}' """.format(clubuser, ligauser, federacionuser)
                else:
                    return """(tabJugador.liga = '{0}')""".format(ligauser)
            # else:
            #    return """ (liga = '{0}') and codigofederacion = '{1}' """.format(ligauser, federacionuser)


def get_permission_query_conditions_clubjugadores(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        ligauser = frappe.defaults.get_user_default('ligauser')
        clubuser = frappe.defaults.get_user_default('clubuser')

        if ligauser:
            if clubuser:
                return """(tabClubJugadores.clubliga='{0}')""".format(clubuser)
            else:
                return """(tabClubJugadores.liga='{0}')""".format(ligauser)

def get_permission_query_conditions_fases(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' )""".format(usuario.federacion,usuario.liga)

def get_permission_query_conditions_disciplina(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' )""".format(usuario.federacion,usuario.liga)

def get_permission_query_conditions_campeonato(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' )""".format(usuario.federacion,usuario.liga)
                

def get_permission_query_conditions_campeonato_categorias(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' )""".format(usuario.federacion,usuario.liga)

def get_permission_query_conditions_directiva_club(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        ligauser = frappe.defaults.get_user_default('ligauser')
        clubuser = frappe.defaults.get_user_default('clubuser')
        if ligauser:
            if clubuser:
                return """(club = '{0}')""".format(clubuser)
            else:
                return """(liga = '{0}')""".format(ligauser)

def get_permission_query_conditions_fixture(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' )""".format(usuario.federacion,usuario.liga)

def get_permission_query_conditions_usuario(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        
        return """(name = '{0}')""".format(frappe.session.user)

def get_permission_query_conditions_usuariosligasequipo(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        usuario =frappe.get_doc("usuarios_ligas_equipo",{"usuario":user})
        if not usuario.club and not usuario.liga and usuario.federacion:
            return """(federacion = '{0}')""".format(usuario.federacion)
        
        if not usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}')""".format(usuario.federacion,usuario.liga)
        
        if usuario.club and usuario.liga and usuario.federacion:
            return """(federacion = '{0}' and liga= '{1}' )""".format(usuario.federacion,usuario.liga)

def get_permission_query_conditions_permisos_usuarios(user):
    if not user:
        user = frappe.session.user
    if 'System Manager' in frappe.get_roles(user):
        return ""
    else:
        return """(user = '{0}')""".format(frappe.session.user)
                
