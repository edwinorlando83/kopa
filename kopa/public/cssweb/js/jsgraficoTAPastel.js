
// Create root and chart
function graficarTAXCAT(datos){
	//am5.ready(function () {
		am5.array.each(am5.registry.rootElements, function(root) {
			if (root) {
				if (root.dom.id == "chartTAXCategorias") {
				  root.dispose();
				}
			}
		  });	
		var root = am5.Root.new("chartTAXCategorias"); 


		root.setThemes([
		  am5themes_Animated.new(root)
		]);


		// Create chart
		// https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
		var chart = root.container.children.push(am5percent.PieChart.new(root, {
		  layout: root.verticalLayout
		}));


		// Create series
		// https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
		var series = chart.series.push(am5percent.PieSeries.new(root, {
		  valueField: "totalTA",
		  categoryField: "Categoria"
		}));


		// Set data
		// https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
		series.data.setAll(datos);


		// Create legend
		// https://www.amcharts.com/docs/v5/charts/percent-charts/legend-percent-series/
		var legend = chart.children.push(am5.Legend.new(root, {
		  centerX: am5.percent(50),
		  x: am5.percent(50),
		  marginTop: 15,
		  marginBottom: 15,
		  
		}));

		legend.data.setAll(series.dataItems);


		// Play initial series animation
		// https://www.amcharts.com/docs/v5/concepts/animations/#Animation_of_series
		series.appear(1000, 100);
	//});
}