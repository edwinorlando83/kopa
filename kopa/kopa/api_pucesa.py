import frappe
from frappe.custom.doctype.property_setter.property_setter import make_property_setter
import json
import random
from datetime import date
from frappe.utils import get_site_name, cstr, encode

@frappe.whitelist(allow_guest=False)
def selectdeportista1():
    data = frappe.db.sql(""" SELECT tj.nombre ,tj.apellidos, tj.liga, tj.club
FROM tabJugador tj  """, as_dict=1)
    return data

@frappe.whitelist(allow_guest=True)
def selectdeportista():
    data = frappe.db.sql(""" SELECT tj.nombre ,tj.apellidos, tj.liga, tj.club
FROM tabJugador tj  """, as_dict=1)
    return data


@frappe.whitelist(allow_guest=True)
def selectliga():
    data = frappe.db.sql(""" SELECT tl.name ,tl.nombre
FROM tabLiga tl  """, as_dict=1)
    return data

@frappe.whitelist(allow_guest=True)
def selectclubbyliga(liga):
    values = {'liga': liga}
    data = frappe.db.sql(""" SELECT tc.name , tc.nombre 
FROM tabClub tc
WHERE tc.liga = %(liga)s """,values=values, as_dict=1)
    return data

@frappe.whitelist(allow_guest=True)
def selectdeportistasclub(club):
    values = {'club': club}
    data = frappe.db.sql(""" SELECT  tj.nombre ,tj.apellidos, tj.liga, tj.club
FROM tabJugador tj 
WHERE tj.club = %(club)s """,values=values, as_dict=1)
    return data
