import frappe
from frappe.custom.doctype.property_setter.property_setter import make_property_setter
import json
from datetime import date


@frappe.whitelist(allow_guest=True)
def get_federacion():
    federacion = frappe.db.sql(
        """ SELECT t.name , t.nombre , t.logo 
 FROM tabfederacion t  """, as_dict=1)
    return federacion
    
@frappe.whitelist()
def get_clubs_campeonato_Cat_grupo(federacion, campeonato,  grupo, fase, disciplina):
    values = {'federacion': federacion, 'campeonato': campeonato, 'disciplina': disciplina,
              'grupo': grupo, 'fase': fase}
    return frappe.db.sql(""" select cj.codigoclub from `tabInscripciones Clubes FEDEPROBAT` cj 
                         where cj.federacion = %(federacion)s and cj.campeonato = %(campeonato)s and 
                         cj.fase= %(fase)s and cj.grupo= %(grupo)s 
                         and cj.disciplina=%(disciplina)s """, values=values, as_dict=0)
    # return frappe.db.sql(""" select cj.club from tabClubJugadores cj where cj.campeonato = $(campeonato)s and cj.codigo_categoria = $(codigo_categoria)s and cj.grupo=$(grupo)s """,values=values,as_dict=0)
    # return frappe.db.get_list('ClubJugadores', {'campeonato':campeonato,'codigo_categoria':categoria,'grupo':grupo}, ['club'])
    # return frappe.db.sql(""" select tc.name,tc.f_inicio_de_inscripciones,tc.f_fin_de_inscripciones from tabCampeonato tc where tc.liga = $(liga)s and tc.activo = 1 """,values=values,as_dict=0)

@frappe.whitelist(allow_guest=True)
def get_categorias(federacion):
    values = {'federacion': federacion}
    categoria = frappe.db.sql(
        """ SELECT tcf.name ,tcf.categoria 
 FROM `tabCategorias FEDEPROBAT` tcf """, values=values, as_dict=1)
    return categoria
    
@frappe.whitelist(allow_guest=True)
def get_fases(federacion):
    values = {'federacion': federacion}
    fasess = frappe.db.sql(
        """ SELECT tff.name, tff.nombre_fase  FROM tabFasesFederacion tff WHERE tff.federacion = %(federacion)s """, values=values, as_dict=1)
    return fasess
    
@frappe.whitelist(allow_guest=True)
def get_grupos(federacion):
    values = {'federacion': federacion}
    gruposs = frappe.db.sql(
        """ SELECT tgf.name, tgf.nombre_grupo  FROM tabGrupoFederacion tgf  WHERE tgf.federacion = %(federacion)s """, values=values, as_dict=1)
    return gruposs

@frappe.whitelist(allow_guest=True)
def get_disciplinas(federacion):
    values = {'federacion': federacion}
    fasess = frappe.db.sql(
        """ SELECT tdf.name,tdf.nombre  FROM `tabDisciplinas FEDEPROBAT` tdf WHERE tdf.federacion  = %(federacion)s """, values=values, as_dict=1)
    return fasess

@frappe.whitelist(allow_guest=True)
def getTablaPosiciones(federacion, grupo, fase, disciplina,categoria):
    values = {'disciplina':disciplina,'federacion':federacion}
    campeonato = frappe.db.sql(
        """ SELECT tedf.name,tedf.nombre  
        FROM `tabEventos Deportivos FEDEPROBAT` tedf 
        WHERE tedf.federacion = %(federacion)s and tedf.activo=1 and
        tedf.disciplina = %(disciplina)s "" """, values=values, as_dict=1)
    values = {'campeonato': campeonato[0].name,
              'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina,'federacion': federacion,}
    nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual  
                                FROM `tabFixture Fedeprobat` tf
                                WHERE tf.federacion = %(federacion)s and 
                                tf.campeonato = %(campeonato)s and
                                tf.categoria = %(categoria)s and
                                tf.grupo = %(grupo)s and
                                tf.fase = %(fase)s and
                                tf.disciplina = %(disciplina)s""", values=values, as_dict=1)
    values = {'parent': nameFicture[0].name}
    fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
                                tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.suspendido
                                FROM tabCalendarioFederacion tc  
                                WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)

    values = {'parent': nameFicture[0].name}
    tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club, tc.logolocal as logo
							FROM tabCalendarioFederacion tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.local!='DESCANSA' AND tc.local!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
    tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club, tc.logovisitante as logo
							FROM tabCalendarioFederacion tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.visitante!='DESCANSA' AND tc.visitante!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
    tblResultados = tblClubFromClendario1 + tblClubFromClendario2

    nfixture = len(fixture)
    nresultados = len(tblResultados)
    for j in range(nresultados):
        tblResultados[j].puntos = 0
        tblResultados[j].gol_a_favor = 0
        tblResultados[j].gol_en_contra = 0
        tblResultados[j].partidos_jugados = 0
        tblResultados[j].puntosascensosancion = 0
        tblResultados[j].partidos_ganados = 0
        tblResultados[j].partidos_perdidos = 0
        tblResultados[j].partidos_empatados = 0

    for i in range(nfixture):
        if (fixture[i].local != "DESCANSA" or fixture[i].local != "D E S C A N S A") and (fixture[i].visitante != "DESCANSA" or fixture[i].visitante != "D E S C A N S A") and fixture[i].jugado_diferido == True:
            for j in range(nresultados):
                if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
                    if fixture[i].marcador_local == fixture[i].marcador_visitante:
                        if fixture[i].local == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 1
                                tblResultados[j].partidos_empatados += 1
                            tblResultados[j].partidos_jugados += 1

                        if fixture[i].visitante == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 1
                                tblResultados[j].partidos_empatados += 1
                            tblResultados[j].partidos_jugados += 1

                    if fixture[i].marcador_local > fixture[i].marcador_visitante:
                        if fixture[i].local == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 3
                                tblResultados[j].partidos_ganados += 1
                            tblResultados[j].partidos_jugados += 1
                        if fixture[i].visitante == tblResultados[j].club:
                            tblResultados[j].partidos_jugados += 1
                    else:
                        tblResultados[j].partidos_perdidos += 1
                    

                    if fixture[i].marcador_local < fixture[i].marcador_visitante:
                        if fixture[i].visitante == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 3
                                tblResultados[j].partidos_ganados += 1
                            tblResultados[j].partidos_jugados += 1
                        if fixture[i].local == tblResultados[j].club:
                            tblResultados[j].partidos_jugados += 1
                
                tblResultados[j].partidos_perdidos = tblResultados[j].partidos_jugados-(tblResultados[j].partidos_ganados+tblResultados[j].partidos_empatados)    

            for j in range(nresultados):
                if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
                    if fixture[i].local == tblResultados[j].club:
                        tblResultados[j].gol_a_favor += fixture[i].marcador_local
                        tblResultados[j].gol_en_contra += fixture[i].marcador_visitante
                    if fixture[i].visitante == tblResultados[j].club:
                        tblResultados[j].gol_a_favor += fixture[i].marcador_visitante
                        tblResultados[j].gol_en_contra += fixture[i].marcador_local

    values = {'federacion': federacion, 'campeonato': campeonato[0].name,
              'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina}

    sancionesxclub = frappe.db.sql("""SELECT tcj.name,tcj.club,tx.puntos , tx.fase ,tx.codigofase 
                                    FROM `tabInscripciones Clubes FEDEPROBAT` tcj,tabSancionesXFaseFederacion tx 
                                    WHERE tcj.federacion = %(federacion)s and 
                                    tcj.campeonato=%(campeonato)s and 
                                    tx.codigofase = %(fase)s and 
                                    tcj.disciplina = %(disciplina)s and
                                    tcj.name = tx.parent 
                                    order by tcj.club """, values=values, as_dict=1)

    nnresultados = len(tblResultados)
    nsanciones = len(sancionesxclub)
    # if 1==1:
    # frappe.throw(str(nsanciones)+" "+liga+" "+categoria+" "+campeonato[0].name+" "+categoria+" "+fase+" "+disciplina)
    for i in range(nsanciones):

        for j in range(nnresultados):
            if sancionesxclub[i].club == tblResultados[j].club:
                tblResultados[j].puntosascensosancion += sancionesxclub[i].puntos

    return tblResultados

#bench --site kopa.website execute kopa.kopa.api_federacion.getTablaPosiciones_app
@frappe.whitelist(allow_guest=True)
#def getTablaPosiciones_app(federacion="FEDEPROBAT", grupo="HHH", fase="FASE-C-00024", disciplina="FEDEPROBAT-FUTBOL 11 SENIOR",categoria="SENIOR"):
def getTablaPosiciones_app(federacion, grupo, fase, disciplina, categoria):
    values = {'disciplina':disciplina,'federacion':federacion}
    campeonato = frappe.db.sql(
        """ SELECT tedf.name,tedf.nombre  
        FROM `tabEventos Deportivos FEDEPROBAT` tedf 
        WHERE tedf.federacion = %(federacion)s and tedf.activo=1 and
        tedf.disciplina = %(disciplina)s "" """, values=values, as_dict=1)
    values = {'campeonato': campeonato[0].name,
              'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina,'federacion': federacion,}
    
    nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual  
                                FROM `tabFixture Fedeprobat` tf
                                WHERE tf.federacion = %(federacion)s and 
                                tf.campeonato = %(campeonato)s and
                                tf.categoria = %(categoria)s and
                                tf.fase = %(fase)s and
                                tf.disciplina = %(disciplina)s""", values=values, as_dict=1)
    
    fixture=[]
    tblResultados=[]
    for i in range(len(nameFicture)):
        values = {'parent': nameFicture[i].name}
        fixture = fixture +frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
                                    tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.suspendido
                                    FROM tabCalendarioFederacion tc  
                                    WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
    
        values = {'parent': nameFicture[i].name}
        tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.club_local,tc.local as club, tc.logolocal as logo,tff.nombregrupo
                                                    FROM tabCalendarioFederacion tc,`tabFixture Fedeprobat` tff
                                                    WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.local!='DESCANSA' AND tc.local!='D E S C A N S A' and tc.parent =tff.name  order by tc.numero_fecha """, values=values, as_dict=1)
    							
        tblClubFromClendario2 = frappe.db.sql("""   SELECT tc.club_visitante,tc.visitante as club, tc.logovisitante as logo,tff.nombregrupo
                                                    FROM tabCalendarioFederacion tc,`tabFixture Fedeprobat` tff
                                                    WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.local!='DESCANSA' AND tc.visitante!='D E S C A N S A' and tc.parent =tff.name  order by tc.numero_fecha """, values=values, as_dict=1)
        tblResultados += tblClubFromClendario1 + tblClubFromClendario2

    
    nfixture = len(fixture)
    nresultados = len(tblResultados)
    for j in range(nresultados):
        tblResultados[j].puntos = 0
        tblResultados[j].gol_a_favor = 0
        tblResultados[j].gol_en_contra = 0
        tblResultados[j].gol_diferencia = 0
        tblResultados[j].partidos_jugados = 0
        tblResultados[j].puntosascensosancion = 0
        tblResultados[j].partidos_ganados = 0
        tblResultados[j].partidos_perdidos = 0
        tblResultados[j].partidos_empatados = 0

    for i in range(nfixture):
        if (fixture[i].local != "DESCANSA" or fixture[i].local != "D E S C A N S A") and (fixture[i].visitante != "DESCANSA" or fixture[i].visitante != "D E S C A N S A") and fixture[i].jugado_diferido == True:
            for j in range(nresultados):
                if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
                    if fixture[i].marcador_local == fixture[i].marcador_visitante:
                        if fixture[i].local == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 1
                                tblResultados[j].partidos_empatados += 1
                            tblResultados[j].partidos_jugados += 1

                        if fixture[i].visitante == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 1
                                tblResultados[j].partidos_empatados += 1
                            tblResultados[j].partidos_jugados += 1

                    if fixture[i].marcador_local > fixture[i].marcador_visitante:
                        if fixture[i].local == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 3
                                tblResultados[j].partidos_ganados += 1
                            tblResultados[j].partidos_jugados += 1
                        if fixture[i].visitante == tblResultados[j].club:
                            tblResultados[j].partidos_jugados += 1
                    else:
                        tblResultados[j].partidos_perdidos += 1
                    

                    if fixture[i].marcador_local < fixture[i].marcador_visitante:
                        if fixture[i].visitante == tblResultados[j].club:
                            if fixture[i].suspendido == 0:
                                tblResultados[j].puntos += 3
                                tblResultados[j].partidos_ganados += 1
                            tblResultados[j].partidos_jugados += 1
                        if fixture[i].local == tblResultados[j].club:
                            tblResultados[j].partidos_jugados += 1
                
                tblResultados[j].partidos_perdidos = tblResultados[j].partidos_jugados-(tblResultados[j].partidos_ganados+tblResultados[j].partidos_empatados)    

            for j in range(nresultados):
                if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
                    if fixture[i].local == tblResultados[j].club:
                        tblResultados[j].gol_a_favor += fixture[i].marcador_local
                        tblResultados[j].gol_en_contra += fixture[i].marcador_visitante
                        tblResultados[j].gol_diferencia=tblResultados[j].gol_a_favor-tblResultados[j].gol_en_contra
                    if fixture[i].visitante == tblResultados[j].club:
                        tblResultados[j].gol_a_favor += fixture[i].marcador_visitante
                        tblResultados[j].gol_en_contra += fixture[i].marcador_local
                        tblResultados[j].gol_diferencia=tblResultados[j].gol_a_favor-tblResultados[j].gol_en_contra

    values = {'federacion': federacion, 'campeonato': campeonato[0].name,
              'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina}

    sancionesxclub = frappe.db.sql("""SELECT tcj.name,tcj.club,tx.puntos , tx.fase ,tx.codigofase 
                                    FROM `tabInscripciones Clubes FEDEPROBAT` tcj,tabSancionesXFaseFederacion tx 
                                    WHERE tcj.federacion = %(federacion)s and 
                                    tcj.campeonato=%(campeonato)s and 
                                    tx.codigofase = %(fase)s and 
                                    tcj.disciplina = %(disciplina)s and
                                    tcj.name = tx.parent 
                                    order by tcj.club """, values=values, as_dict=1)

    nnresultados = len(tblResultados)
    nsanciones = len(sancionesxclub)
    # if 1==1:
    # frappe.throw(str(nsanciones)+" "+liga+" "+categoria+" "+campeonato[0].name+" "+categoria+" "+fase+" "+disciplina)
    for i in range(nsanciones):

        for j in range(nnresultados):
            if sancionesxclub[i].club == tblResultados[j].club:
                tblResultados[j].puntosascensosancion += sancionesxclub[i].puntos

    return tblResultados


@frappe.whitelist(allow_guest=True)
def fixture(federacion, categoria, grupo, fase, disciplina):
    campeonato = frappe.db.get_value("Eventos Deportivos FEDEPROBAT", {'federacion': federacion, 'activo': 1,'disciplina':disciplina})
    # frappe.throw(str(campeonato))
    values = {'federacion':federacion, 'campeonato': campeonato, 'categoria': categoria,
              'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
    return frappe.db.sql("""SELECT tf.campeonato, tf.categoria, tf.grupo, tf.nombrecat, tc.logolocal , tc.`local`,
                         tc.logovisitante,tc.visitante , tc.marcador_local ,tc.marcador_visitante, tc.fecha,
                         tc.hora, tc.nombreestadio,tc.numero_fecha
                         FROM `tabFixture Fedeprobat` tf , tabCalendarioFederacion tc
                         WHERE tf.name = tc.parent AND 
                         tf.federacion=%(federacion)s and
                         tf.campeonato = %(campeonato)s AND 
                         tf.categoria = %(categoria)s AND 
                         tf.grupo = %(grupo)s AND 
                         tf.fase = %(fase)s  and
                         tf.disciplina=%(disciplina)s 
                         ORDER BY tc.numero_fecha ASC
						 """, values=values, as_dict=1)

@frappe.whitelist(allow_guest=True)
def fixture_app(federacion, categoria, grupo, fase, disciplina):
    campeonato = frappe.db.get_value("Eventos Deportivos FEDEPROBAT", {'federacion': federacion, 'activo': 1,'disciplina':disciplina})
    # frappe.throw(str(campeonato))
    values = {'federacion':federacion, 'campeonato': campeonato, 'categoria': categoria,
              'fase': fase, 'disciplina': disciplina}
    return frappe.db.sql("""SELECT tf.campeonato, tf.categoria, tf.grupo,tf.nombregrupo, tf.nombrecat, tc.logolocal , tc.`local`,
                         tc.logovisitante,tc.visitante , tc.marcador_local ,tc.marcador_visitante, tc.fecha,
                         tc.hora, tc.nombreestadio,tc.numero_fecha
                         FROM `tabFixture Fedeprobat` tf , tabCalendarioFederacion tc
                         WHERE tf.name = tc.parent AND 
                         tf.federacion=%(federacion)s and
                         tf.campeonato = %(campeonato)s AND 
                         tf.categoria = %(categoria)s AND 
                         tf.fase = %(fase)s  and
                         tf.disciplina=%(disciplina)s 
                         ORDER BY tc.numero_fecha ASC
						 """, values=values, as_dict=1)