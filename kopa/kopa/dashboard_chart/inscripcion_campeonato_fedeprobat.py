# En tu_app/your_app/dashboard/dashboard.py

import frappe
from frappe import _

def get_participation_data():
    # Consulta para contar la cantidad de jugadores por campeonato
    query = """
        SELECT 
        tic.campeonato,
        COUNT(jcf.name) AS total_jugadores
    FROM 
        `tabInscripciones Clubes FEDEPROBAT` tic
    JOIN 
        tabJugadoresClubFEDEPROBAT jcf ON tic.name = jcf.parent
    GROUP BY 
        tic.campeonato 
    """
    
    data = frappe.db.sql(query, as_dict=True)
    return data

def get_dashboard_data():
    participation_data = get_participation_data()
    
    # Formatear los datos para el gráfico
    labels = [d.campeonato for d in participation_data]
    values = [d.total_jugadores for d in participation_data]
    
    return {
        "title": _("Participación de Jugadores en Campeonatos"),
        "labels": labels,
        "values": values
    }