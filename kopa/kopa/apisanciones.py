from __future__ import unicode_literals
import frappe
 
sitemap = 1

def get_context(context):
	context.show_search = True
	context.allow_guest = True
	context.no_breadcrumbs = True

@frappe.whitelist(allow_guest=True)
def getTablaSanciones(liga,categoria,grupo,fase,disciplina):
	values = {'liga': liga}
	campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.grupo=%(grupo)s and
								tf.fase=%(fase)s  and
        						tf.codigodisciplina=%(disciplina)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v,tc.tal,tc.ta_v,tc.trl,tr_l_2_ps,tr_l_3_ps,tr_l_6_ps,tc.tr_v,tr_v_2_ps,tr_v_3_ps,tr_v_6_ps
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	#values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	#tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club
	#						FROM tabClubJugadores tcj 
	#						WHERE tcj.liga = %(liga)s and 
	#						tcj.campeonato=%(campeonato)s and 
	#						tcj.categoria=%(categoria)s and
	#						tcj.grupo=%(grupo)s and
	#						tcj.fase=%(fase)s and
	#						tcj.codigodisciplina=%(disciplina)s
	#						order by tcj.club """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	nfixture = len(fixture)
	nresultados = len(tblResultados)
 
	sanciones = []
	
	for j in range(nresultados):
		tblResultados[j].ta = 0
		tblResultados[j].tr = 0
		tblResultados[j].tr2ps = 0
		tblResultados[j].tr3ps = 0
		tblResultados[j].tr6ps = 0
		
	for i in range(nfixture):
		if fixture[i].local!="Descansa" and fixture[i].visitante!="Descansa" and fixture[i].jugado_diferido==True:
			if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
				if str(fixture[i].tal)!="None": 
					ta_x_partidostr=str(fixture[i].tal).replace(".",",").replace(",,",",")
					ta_x_partido=ta_x_partidostr.split(",")
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TA"]!="":
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].ta_v)!="None":
					ta_x_partidostr=str(fixture[i].ta_v).replace(".",",").replace(",,",",")
					ta_x_partido=ta_x_partidostr.split(",")
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[0]),"TA":"1","TR":"","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TA"]!="":
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[j]),"TA":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","TR":"","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].trl)!="None": 
					tr_x_partidostr=str(fixture[i].trl).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR"]!="":
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v)!="None":
					tr_x_partidostr=str(fixture[i].tr_v).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR"]!="":
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].tr_l_2_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_2_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR2PS"]!="":
										sanciones[k]["TR2PS"]=str(int(sanciones[k]["TR2PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_2_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_2_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR2PS"]!="":
										sanciones[k]["TR2PS"]=str(int(sanciones[k]["TR2PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].tr_l_3_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_3_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR3PS"]!="":
										sanciones[k]["TR3PS"]=str(int(sanciones[k]["TR3PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_3_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_3_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR3PS"]!="":
										sanciones[k]["TR3PS"]=str(int(sanciones[k]["TR3PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
								
				if str(fixture[i].tr_l_6_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_6_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR6PS"]!="":
										sanciones[k]["TR6PS"]=str(int(sanciones[k]["TR6PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_6_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_6_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR6PS"]!="":
										sanciones[k]["TR6PS"]=str(int(sanciones[k]["TR6PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
	values = {'liga': liga,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club, tjc.nombre,tjc.numero,tjc.darbaja 
									FROM tabClubJugadores tcj,tabCampeonato tc,tabJugadorClub tjc 
									WHERE tcj.liga = %(liga)s and 
									tcj.campeonato = tc.name  and 
									tc.activo = 1 AND 
									
									tcj.codigodisciplina = %(disciplina)s AND 
									tcj.name = tjc.parent and 
									tjc.darbaja = 0
									order by tcj.name """, values=values, as_dict=1)
	resultadoJugadores = len(tblResultados)
	jsondict = sanciones
	for i in range(resultadoJugadores):
		for j in range(len(sanciones)):
			if tblResultados[i].club == jsondict[j]['Club'] and str(tblResultados[i].numero) == str(jsondict[j]['Jugador']):
				jsondict[j]['Nombre'] = tblResultados[i].nombre
	return sanciones

@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
	values = {'liga': liga}
	categoriass = frappe.db.sql(""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)    
	return categoriass

@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
	values = {'liga': liga}
	gruposs = frappe.db.sql(""" SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
	return gruposs

@frappe.whitelist(allow_guest=True)
def get_fases(liga):
	values = {'liga': liga}
	fasess = frappe.db.sql(""" SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)    
	return fasess


@frappe.whitelist(allow_guest=True)
def get_ligas():
	
	ligas = frappe.db.sql(""" SELECT tl.name, tl.nombre  FROM tabLiga tl """, as_dict=1)    
	return ligas

@frappe.whitelist(allow_guest=True)
def get_disciplinas(liga):
    values = {'liga': liga}
    fasess = frappe.db.sql(""" SELECT td.name,td.disciplina  FROM tabDisciplina td WHERE td.liga  = %(liga)s """, values=values, as_dict=1)    
    return fasess