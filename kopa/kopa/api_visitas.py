import frappe
from frappe.custom.doctype.property_setter.property_setter import make_property_setter
from frappe import utils
from frappe.utils import getdate
import json
from datetime import date
import requests


@frappe.whitelist(allow_guest=True)
def registrar_visita(name, ip, pagina, url):
    codigosig = int(name)+1
    fecha = utils.now()
    values = {'name': codigosig, 'fecha': fecha,
              'ip': ip, 'pagina': pagina, 'url': url}
    return frappe.db.sql(""" INSERT INTO tabVisitas(name,fecha, ip, pagina, url) 
						 VALUES(%(name)s,%(fecha)s, %(ip)s, %(pagina)s, %(url)s) """, values=values, as_dict=0)


@frappe.whitelist(allow_guest=True)
def registrar_visitaext():

    data1 = json.loads(frappe.request.get_data())

    # data = json.loads(frappe.request.data)
    data = f"{data1}"
    print(f"{data1}")
    return bytes(data)

    # codigosig = int(data["name"])+1
    # fecha = utils.now()
    # values = {'name': codigosig, 'fecha': fecha,
    #         'ip': data[0].ip, 'pagina': data[0].pagina, "url": data[0].url}
    # return frappe.db.sql(""" INSERT INTO tabVisitas(name,fecha, ip, pagina, url)
    #                     VALUES(%(name)s,%(fecha)s, %(ip)s, %(pagina)s, %(url)s) """, values=values, as_dict=0)


@frappe.whitelist(allow_guest=True)
def get_codigo_visita():

    return frappe.db.sql(""" SELECT MAX(name) as name FROM tabVisitas """, as_dict=1)

# bench --site  kopa execute kopa.kopa.api_visitas.obtenerConteoVisitantesEnRango


@frappe.whitelist(allow_guest=True)
def obtenerConteoVisitantesEnRango(fechaInicio, fechaFin):
    values = {'fechaInicio': getdate(fechaInicio),
              'fechaFin': getdate(fechaFin)}
    return frappe.db.sql(""" SELECT COUNT(DISTINCT ip) AS conteo 
						 FROM tabVisitas 
						 WHERE fecha >= %(fechaInicio)s AND fecha <= %(fechaFin)s """, values=values, as_dict=1)


@frappe.whitelist(allow_guest=True)
def obtenerConteoVisitasEnRango(fechaInicio, fechaFin):
    values = {'fechaInicio': getdate(fechaInicio),
              'fechaFin': getdate(fechaFin)}
    return frappe.db.sql(""" SELECT COUNT(*) AS conteo 
						 FROM tabVisitas 
						 WHERE fecha >= %(fechaInicio)s AND fecha <= %(fechaFin)s """, values=values, as_dict=0)


@frappe.whitelist(allow_guest=True)
def obtenerPaginasVisitadasEnFecha(fecha):
    values = {'fecha': getdate(fecha)}
    return frappe.db.sql(""" SELECT COUNT(*) AS conteo_visitas, count(distinct ip) as conteo_visitantes, url, pagina
	from tabVisitas where fecha = %(fecha)s
	group by url, pagina
	ORDER BY conteo_visitas DESC
	LIMIT 10 """, values=values, as_dict=1)


@frappe.whitelist(allow_guest=True)
def obtenerVisitantesEnRango(fechaInicio, fechaFin):
    values = {'fechaInicio': getdate(fechaInicio),
              'fechaFin': getdate(fechaFin)}
    return frappe.db.sql(""" SELECT fecha, COUNT(DISTINCT ip) AS conteo 
						 FROM tabVisitas 
						 WHERE fecha >= %(fechaInicio)s AND fecha <= %(fechaFin)s 
						 GROUP BY fecha """, values=values, as_dict=1)


@frappe.whitelist(allow_guest=True)
def obtenerVisitasEnRango(fechaInicio="2023-07-01", fechaFin="2023-09-01"):
    values = {'fechaInicio': getdate(fechaInicio),
              'fechaFin': getdate(fechaFin)}
    respuesta = frappe.db.sql("""SELECT fecha, COUNT(*) AS conteo
							FROM tabVisitas 
							WHERE fecha >= %(fechaInicio)s AND fecha <= %(fechaFin)s 
							GROUP BY fecha""", values=values, as_dict=1)
    return respuesta
