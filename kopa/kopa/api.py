import frappe
from frappe.custom.doctype.property_setter.property_setter import make_property_setter
import json
import random
from datetime import date
from frappe.utils import get_site_name, cstr, encode

@frappe.whitelist(allow_guest=True)
def news(name):
	values = {'name': name}
	data = frappe.db.sql(""" SELECT tn.name, tn.titulo, tn.resumen, tn.contenido, tn.imagen, tn.categoria, tn.referencia, tn.autor, tn.contenido, tn.modified 
FROM tabNews tn
WHERE tn.publicado = 1 and
tn.name = %(name)s """,values=values , as_dict=1)
	return {"status": "success", "data": data}

@frappe.whitelist(allow_guest=True)
def news_preview():
	data = frappe.db.sql(""" SELECT tn.name, tn.titulo, tn.resumen, tn.categoria, tn.referencia, tn.autor, tn.imagen, tn.modified 	
FROM tabNews tn
WHERE tn.publicado =1 """, as_dict=1)
	return {"status": "success", "data": data}

@frappe.whitelist(allow_guest=True)
def publicidad():
	data = frappe.db.sql(""" SELECT tp.empresa, tp.publicidad_full ,tp.publicidad_horizontal,tp.publicidad_vertical ,tp.publicidad_opcional ,tp.url, tp.whatsapp
FROM tabPublicidad tp """, as_dict=1)
	return {"status": "success", "data": data}

@frappe.whitelist(allow_guest=True)
def publicidadLigas(liga):
	values = {'name': liga}
	data = frappe.db.sql(""" SELECT tl.name, tl.nombre, tl.empresa, tl.url, tl.whatsapp, tl.publicidad_horizontal,tl.publicidad_horizontal1 
FROM tabLiga tl 
WHERE tl.name= %(name)s """, values=values,as_dict=1)
	return {"status": "success", "data": data}

@frappe.whitelist(allow_guest=True)
def historiajugador(ci):
	values = {'ci': ci}
	data = frappe.db.sql(""" SELECT tj.name as cedula, tj.apellidos as apellidos, tj.nombre as nombres, tj.foto, tj.copia_cedula, tj.carnet_vacunacion, tcj.liga as liga, tcj.club club, tcj.campeonato campeonato,tcj.nombrecampeonato,tcj.logoclub,tcj.logoliga
from `tabJugador` tj
LEFT JOIN `tabJugadorClub` tjc
	on tj.name=tjc.jugador
left join `tabClubJugadores` tcj
	on tjc.parent=tcj.name
WHERE tj.name=%(ci)s """, values=values, as_dict=1)
	return {"status": "success", "data": data}

@frappe.whitelist(allow_guest=True)
def historiajugador(ci):
	values = {'ci': ci}
	data = frappe.db.sql(""" SELECT tj.name as cedula, tj.apellidos as apellidos, tj.nombre as nombres, tj.foto, tj.copia_cedula, tj.carnet_vacunacion, tcj.liga as liga, tcj.club club, tcj.campeonato campeonato,tcj.nombrecampeonato,tcj.logoclub,tcj.logoliga
from `tabJugador` tj
LEFT JOIN `tabJugadorClub` tjc
	on tj.name=tjc.jugador
left join `tabClubJugadores` tcj
	on tjc.parent=tcj.name
WHERE tj.name=%(ci)s """, values=values, as_dict=1)
	return {"status": "success", "data": data}

@frappe.whitelist()
def get_club_jugadores(club):
	# jugadores = frappe.db.sql(f""" SELECT tj.name FROM tabJugador tj WHERE tj.club= = '{club}' """, as_dict=True)
	jugadores = frappe.db.get_list('Jugador', filters={'club': club, 'aprobado': 1}, fields={
		'name', 'nombres_completos', 'nombre', 'apellidos', 'con_sancion', 'aprobado', 'foto', 'copia_cedula', 'carnet_vacunacion'})
	return jugadores


@frappe.whitelist()
def get_campeonato_x_liga1(liga):
	return frappe.db.get_value('Campeonato', {'liga': liga, 'activo': 1}, ['fecha_juveniles'])
	# return frappe.db.sql(""" select tc.name,tc.f_inicio_de_inscripciones,tc.f_fin_de_inscripciones from tabCampeonato tc where tc.liga = $(liga)s and tc.activo = 1 """,values=values,as_dict=0)


@frappe.whitelist()
def filtroJugadorxClub(JugadorClub, txt, jugador, start, page_len, club):
	values = {'club': club}
	return frappe.db.sql(""" select tj.ci,tj.nombres_completos, tj.aprobado 
from tabJugador tj,tabClub tc 
where tc.nombre =%(club)s and
tc.name =tj.club AND 
tj.con_sancion = 0 and 
tj.aprobado=1 """, values=values, as_dict=1)
	# return frappe.db.sql(""" select tj.ci,tj.nombres_completos, tj.aprobado from tabJugador tj where tj.club = %(club)s and tj.con_sancion = 0 and tj.aprobado=1 """,values=values,as_dict=1)
	# return frappe.db.get_list('Jugador', filters={'club':'ATLETICO TUNGURAHUA','con_sancion':0}, fields={'name','nombres_completos'})


@frappe.whitelist()
def filtroJugadoresxClub(club):
	values = {'club': club}
	return frappe.db.sql(""" select tj.ci,tj.nombres_completos, tj.aprobado 
							from tabJugador tj,tabClub tc 
							where tc.nombre = %(club)s and
							tc.name =tj.club AND 
							tj.con_sancion = 0 and 
							tj.aprobado=1 """, values=values, as_dict=1)
	# return frappe.db.sql(""" select tj.ci,tj.nombres_completos, tj.aprobado from tabJugador tj where tj.club = %(club)s and tj.con_sancion = 0 and tj.aprobado=1 """,values=values,as_dict=1)


@frappe.whitelist()
def get_campeonato_liga(campeonato):
	return frappe.db.get_value('Campeonato', {'name': campeonato, 'activo': 1}, ['liga', 'f_inicio_de_inscripciones', 'f_fin_de_inscripciones'])
	# return frappe.db.sql(""" select tc.name,tc.f_inicio_de_inscripciones,tc.f_fin_de_inscripciones from tabCampeonato tc where tc.liga = $(liga)s and tc.activo = 1 """,values=values,as_dict=0)


@frappe.whitelist()
def crear_fixture(clubs):
	""" Create a schedule for the clubs in the list and return it"""
	s = []
	clubs = json.loads(clubs)
	n = len(clubs)
	# frappe.throw(str(n))
	if len(clubs) % 2 == 1:
		clubs = clubs + [{"DESCANSA"}]
	# manipulate map (array of indexes for list) instead of list itself
	# this takes advantage of even/odd indexes to determine home vs. away
	n = len(clubs)
	map = list(range(n))
	mid = n // 2
	for i in range(n-1):
		l1 = map[:mid]
		l2 = map[mid:]
		l2.reverse()
		round = []
		for j in range(mid):
			t1 = clubs[l1[j]]
			t2 = clubs[l2[j]]
			if j == 0 and i % 2 == 1:
				# flip the first match only, every other round
				# (this is because the first match always involves the last player in the list)
				round.append((t2, t1))
			else:
				round.append((t1, t2))
		s.append(round)
		# rotate list by n/2, leaving last element at the end
		map = map[mid:-1] + map[:mid] + map[-1:]

	return s


@frappe.whitelist()
def get_clubs_campeonato_Cat_grupo(federacion, liga, campeonato, categoria, grupo, fase, disciplina):
	values = {"federacion":federacion,'liga': liga, 'campeonato': campeonato, 'codigo_categoria': categoria,
			  'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	return frappe.db.sql(""" select cj.clubliga from tabClubJugadores cj 
	where cj.federacion=%(federacion)s and 
	cj.liga = %(liga)s and 
	cj.campeonato = %(campeonato)s and 
	cj.fase= %(fase)s and 
	cj.grupo= %(grupo)s and 
	cj.codigo_categoria= %(codigo_categoria)s and 
	cj.codigodisciplina=%(disciplina)s """, values=values, as_dict=0)
	# return frappe.db.sql(""" select cj.club from tabClubJugadores cj where cj.campeonato = $(campeonato)s and cj.codigo_categoria = $(codigo_categoria)s and cj.grupo=$(grupo)s """,values=values,as_dict=0)
	# return frappe.db.get_list('ClubJugadores', {'campeonato':campeonato,'codigo_categoria':categoria,'grupo':grupo}, ['club'])
	# return frappe.db.sql(""" select tc.name,tc.f_inicio_de_inscripciones,tc.f_fin_de_inscripciones from tabCampeonato tc where tc.liga = $(liga)s and tc.activo = 1 """,values=values,as_dict=0)


@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
	values = {'liga': liga}
	categoriass = frappe.db.sql(
		""" SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)
	return categoriass


@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
	values = {'liga': liga}
	gruposs = frappe.db.sql(
		""" SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
	return gruposs


@frappe.whitelist(allow_guest=True)
def get_fases(liga):
	values = {'liga': liga}
	fasess = frappe.db.sql(
		""" SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)
	return fasess


@frappe.whitelist(allow_guest=True)
def get_disciplinas(liga):
	values = {'liga': liga}
	fasess = frappe.db.sql(
		""" SELECT td.name,td.disciplina  FROM tabDisciplina td WHERE td.liga  = %(liga)s """, values=values, as_dict=1)
	return fasess
@frappe.whitelist()
def app_get_ligas(federacion):
	values = {'federacion': federacion}
	ligas = frappe.db.sql(
		"""SELECT tl.name, tl.nombre,tl.logo  
FROM tabLiga tl
WHERE tl.federacion = %(federacion)s and tl.solo_inscripcion=0 """,values=values, as_dict=1)
	return ligas

@frappe.whitelist(allow_guest=True)
def get_ligas():
	ligas = frappe.db.sql(
		""" SELECT tl.name, tl.nombre,tl.logo  FROM tabLiga tl """, as_dict=1)
	return ligas


@frappe.whitelist(allow_guest=True)
def getTablaGoleadores(liga, categoria, grupo, fase, disciplina):
	values = {'liga': liga}
	campeonato = frappe.db.sql(
		""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato': campeonato[0].name,
			  'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.grupo=%(grupo)s and
								tf.fase=%(fase)s and
								tf.codigodisciplina=%(disciplina)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	# values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	# tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club
	# FROM tabClubJugadores tcj
	# WHERE tcj.liga = %(liga)s and
	# tcj.campeonato=%(campeonato)s and
	# tcj.categoria=%(categoria)s and
	# tcj.grupo=%(grupo)s and
	# tcj.fase=%(fase)s and
	# tcj.codigodisciplina=%(disciplina)s
	# order by tcj.club """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	nfixture = len(fixture)
	nresultados = len(tblResultados)
	goleadores = []

	for j in range(nresultados):
		tblResultados[j].puntos = 0
		tblResultados[j].gol_a_favor = 0
		tblResultados[j].gol_en_contra = 0
	for i in range(nfixture):
		if fixture[i].local != "DESCANSA" and fixture[i].visitante != "DESCANSA" and fixture[i].jugado_diferido == True:
			if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
				goloes_x_partido = str(fixture[i].golesl).split(",")
				if str(fixture[i].golesl) != "None":
					for j in range(len(goloes_x_partido)):
						total_goledores = len(goleadores)
						if total_goledores < 1:
							goleadores.append({"Club": str(fixture[i].local), "Jugador": str(
								goloes_x_partido[0]), "Goles": "1", "Nombre": ""})
						else:
							existe = "NO"
							for k in range(total_goledores):
								if str(fixture[i].local) == goleadores[k]["Club"] and str(goloes_x_partido[j]) == goleadores[k]["Jugador"]:
									existe = "SI"
									goleadores[k]["Goles"] = str(
										int(goleadores[k]["Goles"])+1)
									break
								else:
									existe = "NO"
							if existe == "NO":
								goleadores.append({"Club": str(fixture[i].local), "Jugador": str(
									goloes_x_partido[j]), "Goles": "1", "Nombre": ""})
				goloes_x_partido = str(fixture[i].goles_v).split(",")
				if str(fixture[i].goles_v) != "None":
					for j in range(len(goloes_x_partido)):
						total_goledores = len(goleadores)
						if total_goledores < 1:
							goleadores.append({"Club": str(fixture[i].visitante), "Jugador": str(
								goloes_x_partido[0]), "Goles": i, "Nombre": ""})
						else:
							existe = "NO"
							for k in range(total_goledores):
								if str(fixture[i].visitante) == goleadores[k]["Club"] and str(goloes_x_partido[j]) == goleadores[k]["Jugador"]:
									existe = "SI"
									goleadores[k]["Goles"] = str(
										int(goleadores[k]["Goles"])+1)
									break
								else:
									existe = "NO"
							if existe == "NO":
								goleadores.append({"Club": str(fixture[i].visitante), "Jugador": str(
									goloes_x_partido[j]), "Goles": "1", "Nombre": ""})

	values = {'liga': liga, 'categoria': categoria,
			  'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club, tjc.nombre,tjc.numero,tjc.darbaja 
									FROM tabClubJugadores tcj,tabCampeonato tc,tabJugadorClub tjc 
									WHERE tcj.liga = %(liga)s and 
									tcj.campeonato = tc.name  and 
									tc.activo = 1 AND 
									tcj.codigodisciplina = %(disciplina)s AND 
									tcj.name = tjc.parent and 
									tjc.darbaja = 0
									order by tcj.name """, values=values, as_dict=1)
	resultadoJugadores = len(tblResultados)
	jsondict = goleadores
	# jsondict.Nombre=""

	# for i in range(resultadoJugadores):
	# for j in range(len(goleadores)):
	# if tblResultados[i].club == goleadores[j]['Club'] and tblResultados[i].numero == goleadores[j]["Jugador"]:
	# goleadores[j]["Nombre"] = tblResultados[i].nombre
	for i in range(resultadoJugadores):
		for j in range(len(goleadores)):
			if tblResultados[i].club == jsondict[j]['Club'] and str(tblResultados[i].numero) == str(jsondict[j]['Jugador']):
				jsondict[j]['Nombre'] = tblResultados[i].nombre
	return jsondict

@frappe.whitelist(allow_guest=True)
#def getTablaGoleadoresGeneral(liga, categoria, grupo, fase, disciplina):
def getTablaGoleadoresGeneral(federacion="FEDEPROBAT",liga="LIGA DEPORTIVA BARRIAL LA PENINSULA", categoria="PRIMERA CATEGORIA", grupo="LDBLP-G-00001", fase="FASE-C-00001", disciplina="DL-00001"):
	#bench --site kopa.website execute kopa.kopa.api.getTablaGoleadoresGeneral
	values = {'liga': liga}
	campeonato = frappe.db.sql(
		""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato': campeonato[0].name,
			  'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.codigodisciplina=%(disciplina)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture=[]
	for j in range(len(nameFicture)):
		values = {'parent': nameFicture[j].name}
		fixture = fixture+frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	#frappe.throw(str(fixture))
	values = {'parent': nameFicture[0].name}
	tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	nfixture = len(fixture)
	nresultados = len(tblResultados)
	goleadores = []

	for j in range(nresultados):
		tblResultados[j].puntos = 0
		tblResultados[j].gol_a_favor = 0
		tblResultados[j].gol_en_contra = 0
	for i in range(nfixture):
		if fixture[i].local != "DESCANSA" and fixture[i].visitante != "DESCANSA" and fixture[i].jugado_diferido == True:
			if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
				goloes_x_partido = str(fixture[i].golesl).split(",")
				if str(fixture[i].golesl) != "None":
					for j in range(len(goloes_x_partido)):
						total_goledores = len(goleadores)
						if total_goledores < 1:
							goleadores.append({"Club": str(fixture[i].local), "Jugador": str(goloes_x_partido[0]), "Goles": "1", "Nombre": "","Foto":""})
						else:
							existe = "NO"
							for k in range(total_goledores):
								if str(fixture[i].local) == goleadores[k]["Club"] and str(goloes_x_partido[j]) == goleadores[k]["Jugador"]:
									existe = "SI"
									goleadores[k]["Goles"] = str(
										int(goleadores[k]["Goles"])+1)
									break
								else:
									existe = "NO"
							if existe == "NO":
								goleadores.append({"Club": str(fixture[i].local), "Jugador": str(goloes_x_partido[j]), "Goles": "1", "Nombre": "","Foto":""})
				goloes_x_partido = str(fixture[i].goles_v).split(",")
				if str(fixture[i].goles_v) != "None":
					for j in range(len(goloes_x_partido)):
						total_goledores = len(goleadores)
						if total_goledores < 1:
							goleadores.append({"Club": str(fixture[i].visitante), "Jugador": str(goloes_x_partido[0]), "Goles": i, "Nombre": "","Foto":""})
						else:
							existe = "NO"
							for k in range(total_goledores):
								if str(fixture[i].visitante) == goleadores[k]["Club"] and str(goloes_x_partido[j]) == goleadores[k]["Jugador"]:
									existe = "SI"
									goleadores[k]["Goles"] = str(
										int(goleadores[k]["Goles"])+1)
									break
								else:
									existe = "NO"
							if existe == "NO":
								goleadores.append({"Club": str(fixture[i].visitante), "Jugador": str(goloes_x_partido[j]), "Goles": "1", "Nombre": "","Foto":""})

	values = {'liga': liga, 'categoria': categoria,
			  'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club, tjc.nombre,tjc.numero,tjc.foto,tjc.darbaja 
									FROM tabClubJugadores tcj,tabCampeonato tc,tabJugadorClub tjc 
									WHERE tcj.liga = %(liga)s and 
									tcj.campeonato = tc.name  and 
									tc.activo = 1 AND 
									tcj.codigodisciplina = %(disciplina)s AND 
									tcj.name = tjc.parent and 
									tjc.darbaja = 0
									order by tcj.name """, values=values, as_dict=1)
	resultadoJugadores = len(tblResultados)
	jsondict = goleadores
	# jsondict.Nombre=""

	# for i in range(resultadoJugadores):
	# for j in range(len(goleadores)):
	# if tblResultados[i].club == goleadores[j]['Club'] and tblResultados[i].numero == goleadores[j]["Jugador"]:
	# goleadores[j]["Nombre"] = tblResultados[i].nombre
	for i in range(resultadoJugadores):
		for j in range(len(goleadores)):
			if tblResultados[i].club == jsondict[j]['Club'] and str(tblResultados[i].numero) == str(jsondict[j]['Jugador']):
				jsondict[j]['Nombre'] = tblResultados[i].nombre
				jsondict[j]['Foto'] = tblResultados[i].foto
                
	return jsondict

@frappe.whitelist(allow_guest=True)
#def getTablaPosiciones(federacion,liga, categoria, grupo, fase, disciplina):
#bench --site kopa.website execute kopa.kopa.api.getTablaPosiciones
def getTablaPosiciones(federacion="FEDEPROBAT",liga="LIGA DEPORTIVA BARRIAL LA PENINSULA", categoria="PRIMERA CATEGORIA", grupo="LDBLP-G-00001", fase="FASE-C-00001", disciplina="DL-00001"):
	values = {'liga': liga,'disciplina':disciplina}
	campeonato = frappe.db.sql(
		""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s and tc.activo=1 and tc.disciplina = %(disciplina)s"" """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato': campeonato[0].name,
			  'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.grupo=%(grupo)s and
								tf.fase=%(fase)s and
								tf.codigodisciplina=%(disciplina)s""", values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.suspendido
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)

	values = {'parent': nameFicture[0].name}
	tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club, tc.logolocal as logo
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.local!='DESCANSA' AND tc.local!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
	tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club, tc.logovisitante as logo
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.visitante!='DESCANSA' AND tc.visitante!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
	tblResultados = tblClubFromClendario1 + tblClubFromClendario2

	nfixture = len(fixture)
	nresultados = len(tblResultados)
	for j in range(nresultados):
		tblResultados[j].puntos = 0
		tblResultados[j].gol_a_favor = 0
		tblResultados[j].gol_en_contra = 0
		tblResultados[j].partidos_jugados = 0
		tblResultados[j].puntosascensosancion = 0
		tblResultados[j].partidos_ganados = 0
		tblResultados[j].partidos_perdidos = 0
		tblResultados[j].partidos_empatados = 0

	for i in range(nfixture):
		if (fixture[i].local != "DESCANSA" or fixture[i].local != "D E S C A N S A") and (fixture[i].visitante != "DESCANSA" or fixture[i].visitante != "D E S C A N S A") and fixture[i].jugado_diferido == True:
			for j in range(nresultados):
				if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
					if fixture[i].marcador_local == fixture[i].marcador_visitante:
						if fixture[i].local == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 1
								tblResultados[j].partidos_empatados += 1
							tblResultados[j].partidos_jugados += 1

						if fixture[i].visitante == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 1
								tblResultados[j].partidos_empatados += 1
							tblResultados[j].partidos_jugados += 1

					if fixture[i].marcador_local > fixture[i].marcador_visitante:
						if fixture[i].local == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 3
								tblResultados[j].partidos_ganados += 1
							tblResultados[j].partidos_jugados += 1
						if fixture[i].visitante == tblResultados[j].club:
							tblResultados[j].partidos_jugados += 1
					else:
						tblResultados[j].partidos_perdidos += 1
					

					if fixture[i].marcador_local < fixture[i].marcador_visitante:
						if fixture[i].visitante == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 3
								tblResultados[j].partidos_ganados += 1
							tblResultados[j].partidos_jugados += 1
						if fixture[i].local == tblResultados[j].club:
							tblResultados[j].partidos_jugados += 1
				
				tblResultados[j].partidos_perdidos = tblResultados[j].partidos_jugados-(tblResultados[j].partidos_ganados+tblResultados[j].partidos_empatados)    

			for j in range(nresultados):
				if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
					if fixture[i].local == tblResultados[j].club:
						tblResultados[j].gol_a_favor += fixture[i].marcador_local
						tblResultados[j].gol_en_contra += fixture[i].marcador_visitante
					if fixture[i].visitante == tblResultados[j].club:
						tblResultados[j].gol_a_favor += fixture[i].marcador_visitante
						tblResultados[j].gol_en_contra += fixture[i].marcador_local

	values = {'liga': liga, 'campeonato': campeonato[0].name,
			  'categoria': categoria, 'grupo': grupo, 'fase': fase, 'disciplina': disciplina}

	sancionesxclub = frappe.db.sql(""" SELECT tcj.name,tcj.club,tx.puntos , tx.fase ,tx.codigofase 
								FROM tabClubJugadores tcj,tabsancionesXfase tx 
								WHERE tcj.liga = %(liga)s and 
								tcj.campeonato=%(campeonato)s and 
								tx.codigofase = %(fase)s and 
								tcj.codigodisciplina = %(disciplina)s and
								tcj.name = tx.parent 
								order by tcj.club """, values=values, as_dict=1)

	nnresultados = len(tblResultados)
	nsanciones = len(sancionesxclub)
	# if 1==1:
	# frappe.throw(str(nsanciones)+" "+liga+" "+categoria+" "+campeonato[0].name+" "+categoria+" "+fase+" "+disciplina)
	for i in range(nsanciones):

		for j in range(nnresultados):
			if sancionesxclub[i].club == tblResultados[j].club:
				tblResultados[j].puntosascensosancion += sancionesxclub[i].puntos

	return tblResultados

#bench --site kopa.website execute kopa.kopa.api.getTablaPosicionesgeneral
@frappe.whitelist(allow_guest=True)
#def getTablaPosicionesgeneral(liga, categoria, fase, disciplina):
#def getTablaPosicionesgeneral(liga="LIGA DEPORTIVA BARRIAL LA PENINSULA", categoria="SEGUNDA CATEGORIA", fase="FASE-C-00003", disciplina="DL-00001"):
def getTablaPosicionesgeneral(liga="LIGA DEPORTIVA BARRIAL LA JOYA", categoria="SEGUNDA CATEGORIA", fase="FASE-C-00023", disciplina="DL-00011"):
	values = {'liga': liga,'disciplina':disciplina}
	campeonato = frappe.db.sql(
		""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s and tc.activo=1 and tc.disciplina = %(disciplina)s"" """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato': campeonato[0].name,
			  'categoria': categoria, 'fase': fase, 'disciplina': disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.fase=%(fase)s and
								tf.codigodisciplina=%(disciplina)s""", values=values, as_dict=1)
	fixture=[]
	tblResultados=[]
	if len(nameFicture)==1:
		values = {'parent': nameFicture[0].name}
		fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.suspendido
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
		values = {'parent': nameFicture[0].name}
		tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club, tc.logolocal as logo
								FROM tabCalendario tc 
								WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.local!='DESCANSA' AND tc.local!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
		tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club, tc.logovisitante as logo
								FROM tabCalendario tc 
								WHERE tc.parent = %(parent)s and numero_fecha=1 and tc.visitante!='DESCANSA' AND tc.visitante!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
		tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	if len(nameFicture)==2:
		values = {'parent': nameFicture[0].name,'parent1': nameFicture[1].name}
		fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.suspendido
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s or tc.parent = %(parent1)s order by tc.numero_fecha """, values=values, as_dict=1)
		tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club, tc.logolocal as logo
								FROM tabCalendario tc 
								WHERE (tc.parent = %(parent)s or tc.parent = %(parent1)s) and numero_fecha=1 and tc.local!='DESCANSA' AND tc.local!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
		tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club, tc.logovisitante as logo
								FROM tabCalendario tc 
								WHERE (tc.parent = %(parent)s or tc.parent = %(parent1)s) and numero_fecha=1 and tc.visitante!='DESCANSA' AND tc.visitante!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
		tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	if len(nameFicture)==3:
		values = {'parent': nameFicture[0].name,'parent1': nameFicture[1].name,'parent2': nameFicture[2].name}
		fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.suspendido
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s or tc.parent = %(parent1)s or tc.parent = %(parent2)s order by tc.numero_fecha """, values=values, as_dict=1)
		tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club, tc.logolocal as logo
								FROM tabCalendario tc 
								WHERE (tc.parent = %(parent)s or tc.parent = %(parent1)s or tc.parent = %(parent2)s) and numero_fecha=1 and tc.local!='DESCANSA' AND tc.local!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
		tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club, tc.logovisitante as logo
								FROM tabCalendario tc 
								WHERE (tc.parent = %(parent)s or tc.parent = %(parent1)s or tc.parent = %(parent2)s) and numero_fecha=1 and tc.visitante!='DESCANSA' AND tc.visitante!='D E S C A N S A' order by tc.numero_fecha """, values=values, as_dict=1)
		tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	
	nfixture = len(fixture)
	nresultados = len(tblResultados)
	for j in range(nresultados):
		tblResultados[j].puntos = 0
		tblResultados[j].gol_a_favor = 0
		tblResultados[j].gol_en_contra = 0
		tblResultados[j].partidos_jugados = 0
		tblResultados[j].puntosascensosancion = 0
		tblResultados[j].partidos_ganados = 0
		tblResultados[j].partidos_perdidos = 0
		tblResultados[j].partidos_empatados = 0

	for i in range(nfixture):
		if (fixture[i].local != "DESCANSA" or fixture[i].local != "D E S C A N S A") and (fixture[i].visitante != "DESCANSA" or fixture[i].visitante != "D E S C A N S A") and fixture[i].jugado_diferido == True:
			for j in range(nresultados):
				if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
					if fixture[i].marcador_local == fixture[i].marcador_visitante:
						if fixture[i].local == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 1
								tblResultados[j].partidos_empatados += 1
							tblResultados[j].partidos_jugados += 1

						if fixture[i].visitante == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 1
								tblResultados[j].partidos_empatados += 1
							tblResultados[j].partidos_jugados += 1

					if fixture[i].marcador_local > fixture[i].marcador_visitante:
						if fixture[i].local == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 3
								tblResultados[j].partidos_ganados += 1
							tblResultados[j].partidos_jugados += 1
						if fixture[i].visitante == tblResultados[j].club:
							tblResultados[j].partidos_jugados += 1
					else:
						tblResultados[j].partidos_perdidos += 1
					

					if fixture[i].marcador_local < fixture[i].marcador_visitante:
						if fixture[i].visitante == tblResultados[j].club:
							if fixture[i].suspendido == 0:
								tblResultados[j].puntos += 3
								tblResultados[j].partidos_ganados += 1
							tblResultados[j].partidos_jugados += 1
						if fixture[i].local == tblResultados[j].club:
							tblResultados[j].partidos_jugados += 1
				
				tblResultados[j].partidos_perdidos = tblResultados[j].partidos_jugados-(tblResultados[j].partidos_ganados+tblResultados[j].partidos_empatados)    

			for j in range(nresultados):
				if fixture[i].numero_fecha <= nameFicture[0].numfechaactual:
					if fixture[i].local == tblResultados[j].club:
						tblResultados[j].gol_a_favor += fixture[i].marcador_local
						tblResultados[j].gol_en_contra += fixture[i].marcador_visitante
					if fixture[i].visitante == tblResultados[j].club:
						tblResultados[j].gol_a_favor += fixture[i].marcador_visitante
						tblResultados[j].gol_en_contra += fixture[i].marcador_local

	values = {'liga': liga, 'campeonato': campeonato[0].name,
			  'categoria': categoria, 'fase': fase, 'disciplina': disciplina}

	sancionesxclub = frappe.db.sql(""" SELECT tcj.name,tcj.club,tx.puntos , tx.fase ,tx.codigofase 
								FROM tabClubJugadores tcj,tabsancionesXfase tx 
								WHERE tcj.liga = %(liga)s and 
								tcj.campeonato=%(campeonato)s and 
								tx.codigofase = %(fase)s and 
								tcj.codigodisciplina = %(disciplina)s and
								tcj.name = tx.parent 
								order by tcj.club """, values=values, as_dict=1)

	nnresultados = len(tblResultados)
	nsanciones = len(sancionesxclub)
	# if 1==1:
	# frappe.throw(str(nsanciones)+" "+liga+" "+categoria+" "+campeonato[0].name+" "+categoria+" "+fase+" "+disciplina)
	for i in range(nsanciones):

		for j in range(nnresultados):
			if sancionesxclub[i].club == tblResultados[j].club:
				tblResultados[j].puntosascensosancion += sancionesxclub[i].puntos

	return tblResultados


@frappe.whitelist(allow_guest=True)
def fixture(federacion,liga, categoria, grupo, fase, disciplina):
	campeonato = frappe.db.get_value("Campeonato", {'liga': liga, 'activo': 1,'disciplina':disciplina})
	# frappe.throw(str(campeonato))
	values = {'federacion':federacion,'liga': liga, 'campeonato': campeonato, 'categoria': categoria,
			  'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	return frappe.db.sql(""" SELECT tf.campeonato, tf.categoria, tf.grupo, tf.nombrecat, tc.logolocal , tc.`local`,
						 tc.logovisitante,tc.visitante , tc.marcador_local ,tc.marcador_visitante, tc.fecha,
						 tc.hora, tc.nombreestadio,tc.numero_fecha
						 FROM tabFixture tf, tabCalendario tc 
						 WHERE tf.name = tc.parent AND 
						 tf.federacion=%(federacion)s and
						 tf.liga = %(liga)s AND 
						 tf.campeonato = %(campeonato)s AND 
						 tf.nombrecat = %(categoria)s AND 
						 tf.grupo = %(grupo)s AND 
						 tf.fase = %(fase)s  and
						 tf.codigodisciplina=%(disciplina)s 
						 ORDER BY tc.numero_fecha ASC """, values=values, as_dict=1)


@frappe.whitelist(allow_guest=True)
def fixturecompleto(liga, fase, disciplina):
	campeonato = frappe.db.get_value("Campeonato", {'liga': liga, 'activo': 1})
	# frappe.throw(str(campeonato))
	values = {'liga': liga, 'campeonato': campeonato,
			  'fase': fase, 'disciplina': disciplina}
	return frappe.db.sql(""" SELECT tf.campeonato, tf.categoria, tf.grupo, tf.nombrecat , tc.`local`,
						 tc.visitante , tc.marcador_local ,tc.marcador_visitante, tc.fecha,
						 tc.hora, tc.nombreestadio,tc.estadio,tc.numero_fecha, tc.vocal
						 FROM tabFixture tf, tabCalendario tc 
						 WHERE tf.name = tc.parent AND 
						 tf.liga = %(liga)s AND 
						 tf.campeonato = %(campeonato)s AND 
						 tf.fase = %(fase)s  and
						 tf.codigodisciplina=%(disciplina)s 
						 ORDER BY tc.estadio ,tc.fecha ,tc.hora,tc.numero_fecha ASC """, values=values, as_dict=1)

@frappe.whitelist(allow_guest=True)
def get_federacion():
	return frappe.db.sql(""" select name,provincia,codigoprovincia,logo FROM tabfederacion """,as_dict=1)

@frappe.whitelist(allow_guest=False)
def get_federacion1(user):
	values={'user':user}
	return frappe.db.sql(""" select federacion from tabusuarios_ligas_equipo where usuario = %(user)s """,values=values,as_dict=0)
	#values = {'club':club}
	##return frappe.db.sql(""" select federacion FROM tabClub where name=%(club)s """,values=values,as_dict=0)


@frappe.whitelist()
def get_clubs_campeonato_Cat_grupo_ia(federacion, liga, campeonato, categoria, grupo, fase, disciplina):
	values = {"federacion":federacion,'liga': liga, 'campeonato': campeonato, 'codigo_categoria': categoria,
			  'grupo': grupo, 'fase': fase, 'disciplina': disciplina}
	return frappe.db.sql(""" select cj.clubliga from tabClubJugadores cj 
	where cj.federacion=%(federacion)s and 
	cj.liga = %(liga)s and 
	cj.campeonato = %(campeonato)s and 
	cj.fase= %(fase)s and 
	cj.grupo= %(grupo)s and 
	cj.codigo_categoria= %(codigo_categoria)s and 
	cj.codigodisciplina=%(disciplina)s """, values=values, as_dict=0)
	
@frappe.whitelist(allow_guest=True)
def generar_calendario_ia(n_clubes=7):
	
	if n_clubes % 2 != 0:
		n_clubes += 1  # Si el número de clubes es impar, agregamos un club adicional ficticio

	# Generar lista de clubes y lista de estadios
	clubes = [f"Equipo {i+1}" for i in range(n_clubes)]
	estadios = [f"Estadio {i+1}" for i in range(n_clubes // 2)]
	horas = [8,10,12,14,16]
	
	s = []
	#clubs = json.loads(clubs)
	n = len(clubes)
	# frappe.throw(str(n))
	if len(clubes) % 2 == 1:
		clubes = clubes + [{"DESCANSA"}]
	# manipulate map (array of indexes for list) instead of list itself
	# this takes advantage of even/odd indexes to determine home vs. away
	n = len(clubes)
	map = list(range(n))
	mid = n // 2
	for i in range(n-1):
		l1 = map[:mid]
		l2 = map[mid:]
		l2.reverse()
		round = []
		for j in range(mid):
			t1 = clubes[l1[j]]
			t2 = clubes[l2[j]]
			if j == 0 and i % 2 == 1:
				# flip the first match only, every other round
				# (this is because the first match always involves the last player in the list)
				round.append((t2, t1))
			else:
				round.append((t1, t2))
		s.append(round)
		# rotate list by n/2, leaving last element at the end
		map = map[mid:-1] + map[:mid] + map[-1:]

	# Crear una lista de partidos
	partidos = s
	for ronda in range(n_clubes - 1):
		for i in range(n_clubes // 2):
			estadio = estadios[i % len(estadios)]
			hora = random.choice(horas)  # Hora aleatoria entre 1 y 24
			partidos.append((estadio, hora,ronda))
			
	frappe.throw(str(partidos))
	return partidos

@frappe.whitelist(allow_guest=True)
def generar_calendario_ia1(clubs):
	n_clubes=len(clubs)
	if n_clubes % 2 != 0:
		n_clubes += 1  # Si el número de clubes es impar, agregamos un club adicional ficticio

	# Generar lista de clubes y lista de estadios
	clubes = [f"Equipo {i+1}" for i in range(n_clubes)] #json.loads(clubs)      #
	estadios = [f"Estadio {i+1}" for i in range(n_clubes // 2)] #["ESTADIO 1","ESTADIO 2","ESTADIO 3"]        #

	# Crear una lista de partidos
	partidos = []
	horas = [8, 10, 12, 14, 16]
	hora_anterior = None
	for ronda in range(n_clubes - 1):
		random.shuffle(clubes)  # Mezclar los clubes para cada ronda
		for i in range(n_clubes // 2):
			local = clubes[i]
			visitante = clubes[n_clubes - i - 1]
			estadio = estadios[i % len(estadios)]
			hora = random.choice(horas)
			while hora == hora_anterior:
				hora = random.choice(horas)
			hora_anterior = hora
			partidos.append((local, visitante, estadio, hora))
	frappe.throw(str(partidos))
	return partidos
# Uso de la función para generar el calendario y mostrarlo en la consola
#calendario = generar_calendario()
#for i, juego in enumerate(calendario):
#    print(f"Juego {i+1}: {juego[0]} vs {juego[1]}, Hora: {juego[2]}, Lugar: {juego[3]}")


@frappe.whitelist()
def set_property(field_name):
	make_property_setter('JugadorClub', field_name, "in_list_view", 0, "Check")

@frappe.whitelist(allow_guest=True)
def getTablaSanciones(liga,categoria,grupo,fase,disciplina):
	values = {'liga': liga}
	campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.grupo=%(grupo)s and
								tf.fase=%(fase)s  and
								tf.codigodisciplina=%(disciplina)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v,tc.tal,tc.ta_v,tc.trl,tr_l_2_ps,tr_l_3_ps,tr_l_6_ps,tc.tr_v,tr_v_2_ps,tr_v_3_ps,tr_v_6_ps
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	#values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	#tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club
	#						FROM tabClubJugadores tcj 
	#						WHERE tcj.liga = %(liga)s and 
	#						tcj.campeonato=%(campeonato)s and 
	#						tcj.categoria=%(categoria)s and
	#						tcj.grupo=%(grupo)s and
	#						tcj.fase=%(fase)s and
	#						tcj.codigodisciplina=%(disciplina)s
	#						order by tcj.club """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	nfixture = len(fixture)
	nresultados = len(tblResultados)
 
	sanciones = []
	
	for j in range(nresultados):
		tblResultados[j].ta = 0
		tblResultados[j].tr = 0
		tblResultados[j].tr2ps = 0
		tblResultados[j].tr3ps = 0
		tblResultados[j].tr6ps = 0
		
	for i in range(nfixture):
		if fixture[i].local!="Descansa" and fixture[i].visitante!="Descansa" and fixture[i].jugado_diferido==True:
			if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
				if str(fixture[i].tal)!="None": 
					ta_x_partidostr=str(fixture[i].tal).replace(".",",").replace(",,",",")
					ta_x_partido=ta_x_partidostr.split(",")
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TA"]!="":
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].ta_v)!="None":
					ta_x_partidostr=str(fixture[i].ta_v).replace(".",",").replace(",,",",")
					ta_x_partido=ta_x_partidostr.split(",")
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[0]),"TA":"1","TR":"","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TA"]!="":
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[j]),"TA":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","TR":"","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].trl)!="None": 
					tr_x_partidostr=str(fixture[i].trl).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR"]!="":
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v)!="None":
					tr_x_partidostr=str(fixture[i].tr_v).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR"]!="":
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].tr_l_2_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_2_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR2PS"]!="":
										sanciones[k]["TR2PS"]=str(int(sanciones[k]["TR2PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_2_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_2_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR2PS"]!="":
										sanciones[k]["TR2PS"]=str(int(sanciones[k]["TR2PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].tr_l_3_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_3_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR3PS"]!="":
										sanciones[k]["TR3PS"]=str(int(sanciones[k]["TR3PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_3_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_3_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR3PS"]!="":
										sanciones[k]["TR3PS"]=str(int(sanciones[k]["TR3PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
								
				if str(fixture[i].tr_l_6_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_6_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR6PS"]!="":
										sanciones[k]["TR6PS"]=str(int(sanciones[k]["TR6PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_6_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_6_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR6PS"]!="":
										sanciones[k]["TR6PS"]=str(int(sanciones[k]["TR6PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
	values = {'liga': liga,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club, tjc.nombre,tjc.numero,tjc.darbaja 
									FROM tabClubJugadores tcj,tabCampeonato tc,tabJugadorClub tjc 
									WHERE tcj.liga = %(liga)s and 
									tcj.campeonato = tc.name  and 
									tc.activo = 1 AND 
									
									tcj.codigodisciplina = %(disciplina)s AND 
									tcj.name = tjc.parent and 
									tjc.darbaja = 0
									order by tcj.name """, values=values, as_dict=1)
	resultadoJugadores = len(tblResultados)
	jsondict = sanciones
	for i in range(resultadoJugadores):
		for j in range(len(sanciones)):
			if tblResultados[i].club == jsondict[j]['Club'] and str(tblResultados[i].numero) == str(jsondict[j]['Jugador']):
				jsondict[j]['Nombre'] = tblResultados[i].nombre
	return sanciones

@frappe.whitelist(allow_guest=True)
#bench --site kopa.website execute kopa.kopa.api.getTablaSancionesGeneral
#def getTablaSancionesGeneral(federacion="FEDEPROBAT",liga="LIGA DEPORTIVA BARRIAL LA PENINSULA", categoria="PRIMERA CATEGORIA", grupo="LDBLP-G-00001", fase="FASE-C-00001", disciplina="DL-00001"):
def getTablaSancionesGeneral(liga,categoria,grupo,fase,disciplina):
	values = {'liga': liga}
	campeonato = frappe.db.sql(""" SELECT tc.name  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
	values = {'liga': liga, 'campeonato':campeonato[0].name,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	nameFicture = frappe.db.sql(""" SELECT tf.name,tf.numfechaactual 
								FROM tabFixture tf 
								WHERE tf.liga = %(liga)s and 
								tf.campeonato=%(campeonato)s and
								tf.nombrecat=%(categoria)s and
								tf.codigodisciplina=%(disciplina)s """, values=values, as_dict=1)
	values = {'parent': nameFicture[0].name}
	fixture=[]
	
	for j in range(len(nameFicture)):
		values = {'parent': nameFicture[j].name}
		fixture = fixture + frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.golesl,tc.goles_v,tc.tal,tc.ta_v,tc.trl,tr_l_2_ps,tr_l_3_ps,tr_l_6_ps,tc.tr_v,tr_v_2_ps,tr_v_3_ps,tr_v_6_ps
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
	
	values = {'parent': nameFicture[0].name}
	tblClubFromClendario1 = frappe.db.sql(""" SELECT tc.local as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblClubFromClendario2 = frappe.db.sql(""" SELECT tc.visitante as club
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s and numero_fecha=1 order by tc.numero_fecha """, values=values, as_dict=1)
	tblResultados = tblClubFromClendario1 + tblClubFromClendario2
	nfixture = len(fixture)
	nresultados = len(tblResultados)
 
	sanciones = []
	
	for j in range(nresultados):
		tblResultados[j].ta = 0
		tblResultados[j].tr = 0
		tblResultados[j].tr2ps = 0
		tblResultados[j].tr3ps = 0
		tblResultados[j].tr6ps = 0
		
	for i in range(nfixture):
		if fixture[i].local!="Descansa" and fixture[i].visitante!="Descansa" and fixture[i].jugado_diferido==True:
			if fixture[i].numero_fecha<=nameFicture[0].numfechaactual:
				if str(fixture[i].tal)!="None": 
					ta_x_partidostr=str(fixture[i].tal).replace(".",",").replace(",,",",")
					ta_x_partido=ta_x_partidostr.split(",")
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TA"]!="":
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(ta_x_partido[j]),"TA":"1","TR":"","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].ta_v)!="None":
					ta_x_partidostr=str(fixture[i].ta_v).replace(".",",").replace(",,",",")
					ta_x_partido=ta_x_partidostr.split(",")
					for j in range(len(ta_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[0]),"TA":"1","TR":"","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(ta_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TA"]!="":
										sanciones[k]["TA"]=str(int(sanciones[k]["TA"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(ta_x_partido[j]),"TA":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","TR":"","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].trl)!="None": 
					tr_x_partidostr=str(fixture[i].trl).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR"]!="":
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v)!="None":
					tr_x_partidostr=str(fixture[i].tr_v).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR"]!="":
										sanciones[k]["TR"]=str(int(sanciones[k]["TR"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"1","TR2PS":"0","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].tr_l_2_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_2_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR2PS"]!="":
										sanciones[k]["TR2PS"]=str(int(sanciones[k]["TR2PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_2_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_2_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR2PS"]!="":
										sanciones[k]["TR2PS"]=str(int(sanciones[k]["TR2PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"1","TR3PS":"0","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
				
				if str(fixture[i].tr_l_3_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_3_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR3PS"]!="":
										sanciones[k]["TR3PS"]=str(int(sanciones[k]["TR3PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_3_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_3_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR3PS"]!="":
										sanciones[k]["TR3PS"]=str(int(sanciones[k]["TR3PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"1","TR6PS":"0","Fecha":fixture[i].numero_fecha, "Nombre":""})
								
				if str(fixture[i].tr_l_6_ps)!="None": 
					tr_x_partidostr=str(fixture[i].tr_l_6_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha,"FechaActual":"", "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].local) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR6PS"]!="":
										sanciones[k]["TR6PS"]=str(int(sanciones[k]["TR6PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].local),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
				if str(fixture[i].tr_v_6_ps)!="None":
					tr_x_partidostr=str(fixture[i].tr_v_6_ps).replace(".",",").replace(",,",",")
					tr_x_partido=tr_x_partidostr.split(",")
					for j in range(len(tr_x_partido)):
						total_sanciones=len(sanciones)
						if total_sanciones<1:
							sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[0]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
						else:
							existe="NO"
							for k in range(total_sanciones):
								if str(fixture[i].visitante) == sanciones[k]["Club"] and str(tr_x_partido[j]) == sanciones[k]["Jugador"]: 
									existe="SI"
									if sanciones[k]["TR6PS"]!="":
										sanciones[k]["TR6PS"]=str(int(sanciones[k]["TR6PS"])+1)
									break
								else:
									existe="NO"
								sanciones[k]["Fecha"]=fixture[i].numero_fecha
							if existe=="NO":
								sanciones.append({"Club":str(fixture[i].visitante),"Jugador":str(tr_x_partido[j]),"TA":"","TR":"0","TR2PS":"0","TR3PS":"0","TR6PS":"1","Fecha":fixture[i].numero_fecha, "Nombre":""})
	
	values = {'liga': liga,'categoria':categoria,'grupo':grupo, 'fase':fase, 'disciplina':disciplina}
	tblResultados = frappe.db.sql(""" SELECT tcj.name,tcj.club, tjc.nombre,tjc.numero,tjc.darbaja 
									FROM tabClubJugadores tcj,tabCampeonato tc,tabJugadorClub tjc 
									WHERE tcj.liga = %(liga)s and 
									tcj.campeonato = tc.name  and 
									tc.activo = 1 AND 
									
									tcj.codigodisciplina = %(disciplina)s AND 
									tcj.name = tjc.parent and 
									tjc.darbaja = 0
									order by tcj.name """, values=values, as_dict=1)
	resultadoJugadores = len(tblResultados)
	jsondict = sanciones
	for i in range(resultadoJugadores):
		for j in range(len(sanciones)):
			if tblResultados[i].club == jsondict[j]['Club'] and str(tblResultados[i].numero) == str(jsondict[j]['Jugador']):
				jsondict[j]['Nombre'] = tblResultados[i].nombre
	return sanciones