# Copyright (c) 2023, kopa and contributors
# For license information, please see license.txt
import frappe
from frappe.model.document import Document

class PasesAprobadosFedeprobat(Document):
	def on_update(self):
		if self.docstatus == 1:
			frappe.db.set_value('JugadoresPasePendiente', self.codigo, 'tesoreriafedeprobat', 1)
			frappe.db.set_value('PasesAprobadosLiga', self.codigo, 'aprobado', 1)
			frappe.throw(self.codigo)
			#values = {'codigo':self.codigo}
			#rappe.db.sql(""" update tabJugadoresPasePendiente 
			#				set tesoreriafedeprobat = 1 
			#				where name =%(codigo)s """,values=values, as_dict=1)