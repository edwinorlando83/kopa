// Copyright (c) 2023, kopa and contributors
// For license information, please see license.txt

frappe.ui.form.on('Disciplina', {
	onload: function (frm) {
	    frm.set_query("liga", function () {
            return {
                filters: [
                    ['federacion', '=', cur_frm.doc.federacion]
                ]
            };
        });
        frm.set_query("disciplina", function () {
			return {
			  filters: [["liga", "=", cur_frm.doc.liga]],
			};
		  });
        
	},
	refresh: function(frm) {
		/*if(frappe.user.has_role("Administrador Club")){
            frm.set_df_property("categoria", "read_only", 1);
            frm.set_df_property("liga", "read_only", 1);
	    }
		if(frappe.user.has_role("Administrador Liga")){
            frm.set_df_property("categoria", "read_only", 0);
            frm.set_df_property("liga", "read_only", 0);
	    }*/
	}
});
