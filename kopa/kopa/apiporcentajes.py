import frappe
import json
from datetime import date



@frappe.whitelist()
def get_context(context):
    context.show_search = True
    context.allow_guest = True
    context.no_breadcrumbs = True


@frappe.whitelist(allow_guest=True)
def get_ligas():

    ligas = frappe.db.sql(
        """ SELECT tl.name, tl.nombre  FROM tabLiga tl """, as_dict=1)
    return ligas


@frappe.whitelist(allow_guest=True)
def get_categorias(liga):
    values = {'liga': liga}
    categoriass = frappe.db.sql(
        """ SELECT tc.name,tc.nombrecat FROM tabcampeonato_categorias tc WHERE tc.liga =%(liga)s """, values=values, as_dict=1)
    return categoriass


@frappe.whitelist(allow_guest=True)
def get_grupos(liga):
    values = {'liga': liga}
    gruposs = frappe.db.sql(
        """ SELECT tgc.name, tgc.nombre_grupo  FROM tabGrupoCategoria tgc  WHERE tgc.liga = %(liga)s """, values=values, as_dict=1)
    return gruposs


@frappe.whitelist(allow_guest=True)
def get_clubs(liga):
    values = {'liga': liga}
    clubes = frappe.db.sql(""" SELECT tcj.club ,tcj.clubliga
								FROM tabClubJugadores tcj,tabcampeonato_categorias tcc,tabLiga tl, tabCampeonato tc
								WHERE tcj.liga = tl.name  and 
								tl.name =%(liga)s and
        						tcj.codigo_categoria = tcc.name and
								tcj.campeonato = tc.name and
								tc.activo =1 """, values=values, as_dict=1)
    return clubes


@frappe.whitelist(allow_guest=True)
def get_fases(liga):
    values = {'liga': liga}
    fasess = frappe.db.sql(
        """ SELECT tcf.name, tcf.nombre_fase  FROM tabCampeonatoFases tcf WHERE tcf.liga = %(liga)s """, values=values, as_dict=1)
    return fasess


# @frappe.whitelist(allow_guest=True)
# def get_clubs(liga,categoria):
# values = {'liga': liga,'categoria':categoria}
# clubs = frappe.db.sql(""" SELECT tcj.clubliga, tcj.club
# FROM tabClubJugadores tcj,tabcampeonato_categorias tcc,tabLiga tl, tabCampeonato tc
# WHERE tcj.liga = tl.name  and
# tl.name =%(liga)s and
# tcj.categoria = %(categoria)s and
# tcj.codigo_categoria = tcc.name and
# tcj.campeonato = tc.name and
# return clubs

@frappe.whitelist(allow_guest=True)
def getTablaPorcentajes(liga, categoria, club, fase, disciplina):
    values = {'liga': liga}
    campeonato = frappe.db.sql(
        """ SELECT tc.name,tc.no_num_camiseta  FROM tabCampeonato tc WHERE tc.liga = %(liga)s  and tc.activo=1 """, values=values, as_dict=1)
    values = {'liga': liga, 'campeonato': campeonato[0].name,
              'categoria': categoria, 'fase': fase, 'disciplina': disciplina, 'club': club}
    nameFicture = frappe.db.sql("""  SELECT DISTINCT(tf.name),tf.numfechaactual 
                                FROM tabFixture tf , tabCalendario tc 
                                WHERE tf.liga = %(liga)s AND 
                                tf.campeonato = %(campeonato)s AND 
                                tf.fase = %(fase)s  and
                                tf.codigodisciplina=%(disciplina)s AND
                                tf.nombrecat=%(categoria)s AND 
                                tf.name = tc.parent AND 
                                (tc.`visitante` =%(club)s OR tc.`local` =%(club)s)
                                 """, values=values, as_dict=1)

    values = {'parent': nameFicture[0].name}
    fixture = frappe.db.sql(""" SELECT tc.name,tc.fecha,tc.hora,tc.local,tc.visitante,tc.marcador_local,
							tc.marcador_visitante,tc.numero_fecha,tc.jugado_diferido,tc.asistencial,tc.asistencia_v
							FROM tabCalendario tc 
							WHERE tc.parent = %(parent)s order by tc.numero_fecha """, values=values, as_dict=1)
    values = {'liga': liga, 'campeonato': campeonato[0].name, 'categoria': categoria, 'club': club.replace(
        "&amp;", "&"), 'fase': fase}
    tblResultadosParent = frappe.db.sql(""" SELECT tcj.name,tcj.club
							FROM tabClubJugadores tcj 
							WHERE tcj.liga = %(liga)s and 
							tcj.campeonato = %(campeonato)s and 
							tcj.club = %(club)s
							order by tcj.club """, values=values, as_dict=1)
    values = {'parent': tblResultadosParent[0].name}
    tblResultados = frappe.db.sql(""" SELECT tjc.jugador,tjc.fecharegistro, tjc.nombre, tjc.numero,tjc.codigojugador
										FROM tabJugadorClub tjc 
										WHERE tjc.parent = %(parent)s AND 
										tjc.darbaja = 0 AND 
										tjc.aprobado =1
									  """, values=values, as_dict=1)
    if campeonato[0].no_num_camiseta == 0:
        nfixture = len(fixture)
        nresultados = len(tblResultados)
        porcentajes = []

        for j in range(nresultados):
            tblResultados[j].jugados = 0
            tblResultados[j].porcentaje = 0

        for i in range(nfixture):
            
            if fixture[i].local != "DESCANSA" and fixture[i].visitante != "DESCANSA" and fixture[i].jugado_diferido == True:
                if int(fixture[i].numero_fecha) <= int(nameFicture[0].numfechaactual):
                    if str(fixture[i].asistencial) != "None" and str(fixture[i].local) == club.replace("&amp;", "&"):
                        asistencia = str(fixture[i].asistencial.replace(".", ",").replace(",,", ",")).split(",")
                        for j in range(len(asistencia)):
                            for k in range(nresultados):
                                if fixture[i].fecha is None:
                                    frappe.throw("La fecha #"+str(fixture[i].numero_fecha)+" no tiene asignada una fecha Calendario. ")
                                if fixture[i].fecha >= tblResultados[k].fecharegistro:  
                                    if int(tblResultados[k].numero) == int(asistencia[j]):
                                        tblResultados[k].jugados = int(tblResultados[k].jugados)+1

                        for k in range(nresultados):
                            if fixture[i].fecha is None:
                                frappe.throw("La fecha #"+str(fixture[i].numero_fecha)+" no tiene asignada una fecha Calendario. ")
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1
                    if str(fixture[i].asistencia_v) != "None" and str(fixture[i].visitante) == club.replace("&amp;", "&"):
                        asistencia = str(fixture[i].asistencia_v.replace(".", ",").replace(",,", ",")).split(",")
                        for j in range(len(asistencia)):
                            for k in range(nresultados):
                                if fixture[i].fecha is None:
                                    frappe.throw("La fecha #"+str(fixture[i].numero_fecha)+" no tiene asignada una fecha Calendario. ")
                                if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                    if int(tblResultados[k].numero) == int(asistencia[j]):
                                        tblResultados[k].jugados = int(tblResultados[k].jugados)+1

                        for k in range(nresultados):
                            frappe.log(str(fixture[i]))
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1
        for i in range(nfixture):
            if fixture[i].local != "DESCANSA" and fixture[i].visitante != "DESCANSA" and fixture[i].jugado_diferido == True:
                if int(fixture[i].numero_fecha) <= int(nameFicture[0].numfechaactual):
                    if str(fixture[i].asistencial) == "None" and str(fixture[i].local) == club.replace("&amp;", "&"):
                        for k in range(nresultados):
                            if fixture[i].fecha is None:
                                frappe.throw("La fecha #"+str(fixture[i].numero_fecha)+" no tiene asignada una fecha Calendario. ")
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1
                    if str(fixture[i].asistencia_v) == "None" and str(fixture[i].visitante) == club.replace("&amp;", "&"):
                        for k in range(nresultados):
                            if fixture[i].fecha is None:
                                frappe.throw("La fecha #"+str(fixture[i].numero_fecha)+" no tiene asignada una fecha Calendario. ")
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1

        for k in range(nresultados):
            if int(tblResultados[k].porcentaje) > 0:
                tblResultados[k].porcentaje = round((int(tblResultados[k].jugados)*100)/int(tblResultados[k].porcentaje), 0)
        return tblResultados
    else:

        nfixture = len(fixture)
        nresultados = len(tblResultados)
        porcentajes = []

        for j in range(nresultados):
            tblResultados[j].jugados = 0
            tblResultados[j].porcentaje = 0

        for i in range(nfixture):
            
            if fixture[i].local != "DESCANSA" and fixture[i].visitante != "DESCANSA" and fixture[i].jugado_diferido == True:
                if int(fixture[i].numero_fecha) <= int(nameFicture[0].numfechaactual):
                    if str(fixture[i].asistencial) != "None" and str(fixture[i].local) == club.replace("&amp;", "&"):
                        asistencia = str(fixture[i].asistencial.replace(".", ",").replace(",,", ",")).split(",")
                        for j in range(len(asistencia)):
                            for k in range(nresultados):
                                if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                    if str(tblResultados[k].codigojugador) == str(asistencia[j]):
                                        tblResultados[k].jugados = int(tblResultados[k].jugados)+1

                        for k in range(nresultados):
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1
                    if str(fixture[i].asistencia_v) != "None" and str(fixture[i].visitante) == club.replace("&amp;", "&"):
                        asistencia = str(fixture[i].asistencia_v.replace(".", ",").replace(",,", ",")).split(",")
                        for j in range(len(asistencia)):
                            for k in range(nresultados):
                                if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                    if str(tblResultados[k].codigojugador) == str(asistencia[j]):
                                        tblResultados[k].jugados = int(tblResultados[k].jugados)+1

                        for k in range(nresultados):
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1
        for i in range(nfixture):
            if fixture[i].local != "DESCANSA" and fixture[i].visitante != "DESCANSA" and fixture[i].jugado_diferido == True:
                if int(fixture[i].numero_fecha) <= int(nameFicture[0].numfechaactual):
                    if str(fixture[i].asistencial) == "None" and str(fixture[i].local) == club.replace("&amp;", "&"):
                        for k in range(nresultados):
                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(tblResultados[k].porcentaje)+1
                    if str(fixture[i].asistencia_v) == "None" and str(fixture[i].visitante) == club.replace("&amp;", "&"):
                        for k in range(nresultados):

                            if fixture[i].fecha >= tblResultados[k].fecharegistro:
                                tblResultados[k].porcentaje = int(
                                    tblResultados[k].porcentaje)+1

        for k in range(nresultados):
            if int(tblResultados[k].porcentaje) > 0:
                tblResultados[k].porcentaje = round(
                    (int(tblResultados[k].jugados)*100)/int(tblResultados[k].porcentaje), 0)
        return tblResultados
