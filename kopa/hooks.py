# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version
from frappe import _  # Importamos la función de traducción

app_name = "kopa"
app_title = "Kopa"
app_publisher = "kopa"
app_description = "kopa"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "edwinorlando83@gmail.com"
app_license = "MIT"
app_logo_url = "/assets/kopa/logo.png"


# Includes in <head>
# ------------------
website_context = {
    "favicon": "/assets/kopa/img/logo.ico",
    "splash_image": "/assets/kopa/img/logo.png"
}

on_session_creation = [
    #"kopa.fedeprobat.doctype.liga.liga.setliga"
]
# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/kopa/css/kopa.css"
# app_include_js = "/assets/kopa/js/kopa.js"

# include js, css files in header of web template
# web_include_css = "/assets/kopa/css/kopa.css"
# web_include_js = "/assets/kopa/js/kopa.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
# "Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "kopa.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "kopa.install.before_install"
# after_install = "kopa.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "kopa.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }

permission_query_conditions = {
    "federacion": "kopa.filtros.filtros.get_permission_query_conditions_federacion",
    "Liga": "kopa.filtros.filtros.get_permission_query_conditions_liga",
    "Club": "kopa.filtros.filtros.get_permission_query_conditions_club",
    "Jugador": "kopa.filtros.filtros.get_permission_query_conditions_jugador",
    "ClubJugadores": "kopa.filtros.filtros.get_permission_query_conditions_clubjugadores",
    "campeonato_categorias": "kopa.filtros.filtros.get_permission_query_conditions_campeonato_categorias",
    "CampeonatoFases": "kopa.filtros.filtros.get_permission_query_conditions_fases",
    "Disciplina": "kopa.filtros.filtros.get_permission_query_conditions_disciplina",
    "Campeonato": "kopa.filtros.filtros.get_permission_query_conditions_campeonato",
    #"Liga": "kopa.filtros.filtros.get_permission_query_conditions_ligas",
    "Fixture": "kopa.filtros.filtros.get_permission_query_conditions_fixture",
    "User": "kopa.filtros.filtros.get_permission_query_conditions_usuario",
    "DirectivaClub": "kopa.filtros.filtros.get_permission_query_conditions_directiva_club",
    "usuarios_ligas_equipo": "kopa.filtros.filtros.get_permission_query_conditions_usuariosligasequipo",
    "User Permission": "kopa.filtros.filtros.get_permission_query_conditions_permisos_usuarios"
}

# Agregar el dashboard
#dashboard_deportistas_federacion = {
#    "module": "Kopa",
#    "label": _("Participación de Jugadores"),
#    "data": "kopa.dashboard_chart.inscripcion_campeonato_fedeprobat.get_dashboard_data"
#}

# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
# }
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"kopa.tasks.all"
# 	],
# 	"daily": [
# 		"kopa.tasks.daily"
# 	],
# 	"hourly": [
# 		"kopa.tasks.hourly"
# 	],
# 	"weekly": [
# 		"kopa.tasks.weekly"
# 	]
# 	"monthly": [
# 		"kopa.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "kopa.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "kopa.event.get_events"
# }

#app_include_js = "/assets/js/plugins/jquery/jquery.min.js"